type GraphEdge = {
  id: string;
  from: string;
  to: string;
  hidden?: boolean;
  value?: number;
  type?: string;
};
