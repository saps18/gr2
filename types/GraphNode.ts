type GraphNode = {
  id: string;
  label: string;
  hidden: boolean;
  group?: string;
  physics?: boolean;
  shape?: string;
  size?: number;
  color?: string;
  fixed?: boolean;
  x?: number;
  y?: number;
};
