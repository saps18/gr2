type Graph = {
  skillNodes: GraphNode[];
  roleNodes: GraphNode[];
  bmkNodes: GraphNode[];
  frameworkNodes: GraphNode[];
  neNodes: GraphNode[];
  roleEdges: GraphEdge[];
  skillEdges: GraphEdge[];
  bmkEdges: GraphEdge[];
  neEdges: GraphEdge[];
};
