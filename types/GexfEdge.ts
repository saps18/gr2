type GexfEdge = {
  _attributes: {
    source: string;
    target: string;
    weight: string;
  };
};
