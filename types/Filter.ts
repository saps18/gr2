type Filter = {
  id: string;
  name: string;
  options: {
    key: string;
    title: string;
  }[];
};
