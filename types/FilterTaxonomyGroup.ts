type FilterTaxonomyGroup = {
  title: string;
  nodes: { id: string; title: string }[];
};
