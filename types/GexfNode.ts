type GexfNode = {
  attvalues: {
    attvalue: {
      _attributes: {
        for: string;
        value: string;
      };
    }[];
  };
  'viz:color': {
    _attributes: {
      r: string;
      g: string;
      b: string;
    };
  };
  _attributes: {
    id: string;
    label: string;
    weight: string;
  };
};
