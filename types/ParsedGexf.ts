type ParsedGexf = {
  _declaration: any;
  graphview: {
    graph: {
      nodes: { node: GexfNode[] };
      edges: { edge: GexfEdge[] };
    };
  };
};
