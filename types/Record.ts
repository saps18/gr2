type Record<K extends string | number | symbol, T> = { [P in K]: T };

export default Record;
