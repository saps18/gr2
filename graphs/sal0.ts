const sal0 ={
    "roleNodes": [
      {
        "id": "PR специалист",
        "label": "PR специалист"
      },
      {
        "id": "Маркетолог",
        "label": "Маркетолог"
      },
      {
        "id": "Менеджер по продажам",
        "label": "Менеджер по продажам"
      }
    ],
    "skillNodes": [
      {
        "id": "Продажи",
        "label": "Продажи",
        "type": "hard skill"
      },
      {
        "id": "Организаторские способности",
        "label": "Организаторские способности",
        "type": "soft skill"
      },
      {
        "id": "Ведение переговоров",
        "label": "Ведение переговоров",
        "type": "soft skill"
      },
      {
        "id": "Коммуникабельность",
        "label": "Коммуникабельность",
        "type": "soft skill"
      },
      {
        "id": "Работа с клиентами",
        "label": "Работа с клиентами",
        "type": "hard skill"
      },
      {
        "id": "Письменная коммуникация",
        "label": "Письменная коммуникация",
        "type": "soft skill"
      },
      {
        "id": "Маркетинг",
        "label": "Маркетинг",
        "type": "hard skill"
      },
      {
        "id": "Реклама",
        "label": "Реклама",
        "type": "hard skill"
      },
      {
        "id": "Обучаемость",
        "label": "Обучаемость",
        "type": "soft skill"
      },
      {
        "id": "Проектирование",
        "label": "Проектирование",
        "type": "soft skill"
      },
      {
        "id": "Пользовательский сервис",
        "label": "Пользовательский сервис",
        "type": "hard skill"
      },
      {
        "id": "Инициативность",
        "label": "Инициативность",
        "type": "soft skill"
      },
      {
        "id": "Проектная деятельность",
        "label": "Проектная деятельность",
        "type": "hard skill"
      },
      {
        "id": "Программирование",
        "label": "Программирование",
        "type": "hard skill"
      },
      {
        "id": "Бухгалтерский учет",
        "label": "Бухгалтерский учет",
        "type": "hard skill"
      },
      {
        "id": "Управление проектами",
        "label": "Управление проектами",
        "type": "hard skill"
      },
      {
        "id": "Цифровая грамотность",
        "label": "Цифровая грамотность",
        "type": "hard skill"
      },
      {
        "id": "Маркетинг",
        "label": "Маркетинг",
        "type": "hard skill"
      },
      {
        "id": "Продажи",
        "label": "Продажи",
        "type": "hard skill"
      },
      {
        "id": "Реклама",
        "label": "Реклама",
        "type": "hard skill"
      },
      {
        "id": "Организаторские способности",
        "label": "Организаторские способности",
        "type": "soft skill"
      },
      {
        "id": "Проектная деятельность",
        "label": "Проектная деятельность",
        "type": "hard skill"
      },
      {
        "id": "Ведение переговоров",
        "label": "Ведение переговоров",
        "type": "soft skill"
      },
      {
        "id": "Управление проектами",
        "label": "Управление проектами",
        "type": "hard skill"
      },
      {
        "id": "Программирование",
        "label": "Программирование",
        "type": "hard skill"
      },
      {
        "id": "Проектирование",
        "label": "Проектирование",
        "type": "soft skill"
      },
      {
        "id": "Маркетинговые исследования",
        "label": "Маркетинговые исследования",
        "type": "hard skill"
      },
      {
        "id": "Коммуникабельность",
        "label": "Коммуникабельность",
        "type": "soft skill"
      },
      {
        "id": "Инициативность",
        "label": "Инициативность",
        "type": "soft skill"
      },
      {
        "id": "Креативность",
        "label": "Креативность",
        "type": "soft skill"
      },
      {
        "id": "Работа с клиентами",
        "label": "Работа с клиентами",
        "type": "hard skill"
      },
      {
        "id": "Письменная коммуникация",
        "label": "Письменная коммуникация",
        "type": "soft skill"
      },
      {
        "id": "Аналитическое мышление",
        "label": "Аналитическое мышление",
        "type": "soft skill"
      },
      {
        "id": "Стратегическое планирование",
        "label": "Стратегическое планирование",
        "type": "hard skill"
      },
      {
        "id": "Анализ данных",
        "label": "Анализ данных",
        "type": "hard skill"
      },
      {
        "id": "Системная интеграция",
        "label": "Системная интеграция",
        "type": "hard skill"
      },
      {
        "id": "Прогнозирование",
        "label": "Прогнозирование",
        "type": "soft skill"
      },
      {
        "id": "Продажи",
        "label": "Продажи",
        "type": "hard skill"
      },
      {
        "id": "Ведение переговоров",
        "label": "Ведение переговоров",
        "type": "soft skill"
      },
      {
        "id": "Работа с клиентами",
        "label": "Работа с клиентами",
        "type": "hard skill"
      },
      {
        "id": "Организаторские способности",
        "label": "Организаторские способности",
        "type": "soft skill"
      },
      {
        "id": "Коммуникабельность",
        "label": "Коммуникабельность",
        "type": "soft skill"
      },
      {
        "id": "Маркетинг",
        "label": "Маркетинг",
        "type": "hard skill"
      },
      {
        "id": "Письменная коммуникация",
        "label": "Письменная коммуникация",
        "type": "soft skill"
      },
      {
        "id": "Реклама",
        "label": "Реклама",
        "type": "hard skill"
      },
      {
        "id": "Обучаемость",
        "label": "Обучаемость",
        "type": "soft skill"
      },
      {
        "id": "Инициативность",
        "label": "Инициативность",
        "type": "soft skill"
      },
      {
        "id": "Работа с возражениями",
        "label": "Работа с возражениями",
        "type": "soft skill"
      },
      {
        "id": "Проектная деятельность",
        "label": "Проектная деятельность",
        "type": "hard skill"
      },
      {
        "id": "Маркетинговые исследования",
        "label": "Маркетинговые исследования",
        "type": "hard skill"
      }
    ],
    "skillEdges": [
      {
        "id": "PR специалист_Продажи",
        "from": "PR специалист",
        "to": "Продажи",
        "weight": 17.03939209
      },
      {
        "id": "PR специалист_Организаторские способности",
        "from": "PR специалист",
        "to": "Организаторские способности",
        "weight": 7.490599839
      },
      {
        "id": "PR специалист_Ведение переговоров",
        "from": "PR специалист",
        "to": "Ведение переговоров",
        "weight": 5.715888734
      },
      {
        "id": "PR специалист_Коммуникабельность",
        "from": "PR специалист",
        "to": "Коммуникабельность",
        "weight": 5.47768569
      },
      {
        "id": "PR специалист_Работа с клиентами",
        "from": "PR специалист",
        "to": "Работа с клиентами",
        "weight": 4.895958423
      },
      {
        "id": "PR специалист_Письменная коммуникация",
        "from": "PR специалист",
        "to": "Письменная коммуникация",
        "weight": 3.531704628
      },
      {
        "id": "PR специалист_Маркетинг",
        "from": "PR специалист",
        "to": "Маркетинг",
        "weight": 3.061204402
      },
      {
        "id": "PR специалист_Реклама",
        "from": "PR специалист",
        "to": "Реклама",
        "weight": 2.755083962
      },
      {
        "id": "PR специалист_Обучаемость",
        "from": "PR специалист",
        "to": "Обучаемость",
        "weight": 2.265881843
      },
      {
        "id": "PR специалист_Проектирование",
        "from": "PR специалист",
        "to": "Проектирование",
        "weight": 1.950902612
      },
      {
        "id": "PR специалист_Пользовательский сервис",
        "from": "PR специалист",
        "to": "Пользовательский сервис",
        "weight": 1.681201646
      },
      {
        "id": "PR специалист_Инициативность",
        "from": "PR специалист",
        "to": "Инициативность",
        "weight": 1.586707876
      },
      {
        "id": "PR специалист_Проектная деятельность",
        "from": "PR специалист",
        "to": "Проектная деятельность",
        "weight": 1.4233124
      },
      {
        "id": "PR специалист_Программирование",
        "from": "PR специалист",
        "to": "Программирование",
        "weight": 1.167391775
      },
      {
        "id": "PR специалист_Бухгалтерский учет",
        "from": "PR специалист",
        "to": "Бухгалтерский учет",
        "weight": 1.140815402
      },
      {
        "id": "PR специалист_Управление проектами",
        "from": "PR специалист",
        "to": "Управление проектами",
        "weight": 1.066992145
      },
      {
        "id": "PR специалист_Цифровая грамотность",
        "from": "PR специалист",
        "to": "Цифровая грамотность",
        "weight": 1.047305943
      },
      {
        "id": "Маркетолог_Маркетинг",
        "from": "Маркетолог",
        "to": "Маркетинг",
        "weight": 7.140694298
      },
      {
        "id": "Маркетолог_Продажи",
        "from": "Маркетолог",
        "to": "Продажи",
        "weight": 6.674028819
      },
      {
        "id": "Маркетолог_Реклама",
        "from": "Маркетолог",
        "to": "Реклама",
        "weight": 6.451383075
      },
      {
        "id": "Маркетолог_Организаторские способности",
        "from": "Маркетолог",
        "to": "Организаторские способности",
        "weight": 5.583955257
      },
      {
        "id": "Маркетолог_Проектная деятельность",
        "from": "Маркетолог",
        "to": "Проектная деятельность",
        "weight": 3.14553907
      },
      {
        "id": "Маркетолог_Ведение переговоров",
        "from": "Маркетолог",
        "to": "Ведение переговоров",
        "weight": 3.095666423
      },
      {
        "id": "Маркетолог_Управление проектами",
        "from": "Маркетолог",
        "to": "Управление проектами",
        "weight": 2.668186595
      },
      {
        "id": "Маркетолог_Программирование",
        "from": "Маркетолог",
        "to": "Программирование",
        "weight": 2.311953405
      },
      {
        "id": "Маркетолог_Проектирование",
        "from": "Маркетолог",
        "to": "Проектирование",
        "weight": 1.987781202
      },
      {
        "id": "Маркетолог_Маркетинговые исследования",
        "from": "Маркетолог",
        "to": "Маркетинговые исследования",
        "weight": 1.968188376
      },
      {
        "id": "Маркетолог_Коммуникабельность",
        "from": "Маркетолог",
        "to": "Коммуникабельность",
        "weight": 1.948595551
      },
      {
        "id": "Маркетолог_Инициативность",
        "from": "Маркетолог",
        "to": "Инициативность",
        "weight": 1.75088613
      },
      {
        "id": "Маркетолог_Креативность",
        "from": "Маркетолог",
        "to": "Креативность",
        "weight": 1.692107654
      },
      {
        "id": "Маркетолог_Работа с клиентами",
        "from": "Маркетолог",
        "to": "Работа с клиентами",
        "weight": 1.553176709
      },
      {
        "id": "Маркетолог_Письменная коммуникация",
        "from": "Маркетолог",
        "to": "Письменная коммуникация",
        "weight": 1.512209893
      },
      {
        "id": "Маркетолог_Аналитическое мышление",
        "from": "Маркетолог",
        "to": "Аналитическое мышление",
        "weight": 1.401777604
      },
      {
        "id": "Маркетолог_Стратегическое планирование",
        "from": "Маркетолог",
        "to": "Стратегическое планирование",
        "weight": 1.382184778
      },
      {
        "id": "Маркетолог_Анализ данных",
        "from": "Маркетолог",
        "to": "Анализ данных",
        "weight": 1.286001817
      },
      {
        "id": "Маркетолог_Системная интеграция",
        "from": "Маркетолог",
        "to": "Системная интеграция",
        "weight": 1.257503162
      },
      {
        "id": "Маркетолог_Прогнозирование",
        "from": "Маркетолог",
        "to": "Прогнозирование",
        "weight": 1.050887911
      },
      {
        "id": "Менеджер по продажам_Продажи",
        "from": "Менеджер по продажам",
        "to": "Продажи",
        "weight": 24.05423416
      },
      {
        "id": "Менеджер по продажам_Ведение переговоров",
        "from": "Менеджер по продажам",
        "to": "Ведение переговоров",
        "weight": 12.56493063
      },
      {
        "id": "Менеджер по продажам_Работа с клиентами",
        "from": "Менеджер по продажам",
        "to": "Работа с клиентами",
        "weight": 7.727598934
      },
      {
        "id": "Менеджер по продажам_Организаторские способности",
        "from": "Менеджер по продажам",
        "to": "Организаторские способности",
        "weight": 5.4601531
      },
      {
        "id": "Менеджер по продажам_Коммуникабельность",
        "from": "Менеджер по продажам",
        "to": "Коммуникабельность",
        "weight": 5.154295674
      },
      {
        "id": "Менеджер по продажам_Маркетинг",
        "from": "Менеджер по продажам",
        "to": "Маркетинг",
        "weight": 3.81040257
      },
      {
        "id": "Менеджер по продажам_Письменная коммуникация",
        "from": "Менеджер по продажам",
        "to": "Письменная коммуникация",
        "weight": 3.279851001
      },
      {
        "id": "Менеджер по продажам_Реклама",
        "from": "Менеджер по продажам",
        "to": "Реклама",
        "weight": 2.827899665
      },
      {
        "id": "Менеджер по продажам_Обучаемость",
        "from": "Менеджер по продажам",
        "to": "Обучаемость",
        "weight": 2.058129998
      },
      {
        "id": "Менеджер по продажам_Инициативность",
        "from": "Менеджер по продажам",
        "to": "Инициативность",
        "weight": 1.941084
      },
      {
        "id": "Менеджер по продажам_Работа с возражениями",
        "from": "Менеджер по продажам",
        "to": "Работа с возражениями",
        "weight": 1.135431618
      },
      {
        "id": "Менеджер по продажам_Проектная деятельность",
        "from": "Менеджер по продажам",
        "to": "Проектная деятельность",
        "weight": 1.091005399
      },
      {
        "id": "Менеджер по продажам_Маркетинговые исследования",
        "from": "Менеджер по продажам",
        "to": "Маркетинговые исследования",
        "weight": 1.026929123
      }
    ],
    "bmkNodes": [
      {
        "id": "Универсальные компетенции",
        "label": "Универсальные компетенции"
      },
      {
        "id": "Работа с новизной",
        "label": "Работа с новизной"
      },
      {
        "id": "Мышление",
        "label": "Мышление"
      },
      {
        "id": "Самоактуализация",
        "label": "Самоактуализация"
      },
      {
        "id": "Взаимодействие",
        "label": "Взаимодействие"
      },
      {
        "id": "Саморегуляция",
        "label": "Саморегуляция"
      },
      {
        "id": "Базовые грамотности",
        "label": "Базовые грамотности"
      },
      {
        "id": "Математическая грамотность",
        "label": "Математическая\nграмотность"
      },
      {
        "id": "Грамотность современных технологий",
        "label": "Грамотность\nсовременных\nтехнологий"
      },
      {
        "id": "Грамотность цифровых сред и интерфейсов",
        "label": "Грамотность цифровых\nсред и интерфейсов"
      },
      {
        "id": "Грамотность синтаксисов программирования",
        "label": "Грамотность\nсинтаксисов\nпрограммирования"
      },
      {
        "id": "Грамотность письменной и устной коммуникации",
        "label": "Грамотность письменной\nи устной коммуникации"
      },
      {
        "id": "Грамотность онлайн взаимодействия",
        "label": "Грамотность\nонлайн\nвзаимодействия"
      },
      {
        "id": "Финансово-экономическая грамотность",
        "label": "Финансово-экономическая\nграмотность"
      },
      {
        "id": "Грамотность работы с данными и информацией",
        "label": "Грамотность работы\nс данными\nи информацией"
      },
      {
        "id": "Специальные компетенции",
        "label": "Специальные компетенции"
      },
      {
        "id": "Цифровое мышление",
        "label": "Цифровое мышление"
      },
      {
        "id": "Работа с данными",
        "label": "Работа с данными"
      },
      {
        "id": "Создание цифрового продукта",
        "label": "Создание\nцифрового\nпродукта"
      },
      {
        "id": "Ведение IT бизнеса",
        "label": "Ведение IT бизнеса"
      },
      {
        "id": "Работа с будущим",
        "label": "Работа с будущим"
      }
    ],
    "bmkEdges": [
      {
        "id": "Работа с новизной1",
        "from": "Универсальные компетенции",
        "to": "Работа с новизной",
        "value": 1
      },
      {
        "id": "Самоактуализация1",
        "from": "Универсальные компетенции",
        "to": "Самоактуализация",
        "value": 1
      },
      {
        "id": "Мышление1",
        "from": "Универсальные компетенции",
        "to": "Мышление",
        "value": 1
      },
      {
        "id": "Взаимодействие1",
        "from": "Универсальные компетенции",
        "to": "Взаимодействие",
        "value": 1
      },
      {
        "id": "Саморегуляция1",
        "from": "Универсальные компетенции",
        "to": "Саморегуляция",
        "value": 1
      },
      {
        "id": "Математическая грамотность1",
        "from": "Базовые грамотности",
        "to": "Математическая грамотность",
        "value": 1
      },
      {
        "id": "Грамотность современных технологий1",
        "from": "Базовые грамотности",
        "to": "Грамотность современных технологий",
        "value": 1
      },
      {
        "id": "Грамотность цифровых сред и интерфейсов1",
        "from": "Базовые грамотности",
        "to": "Грамотность цифровых сред и интерфейсов",
        "value": 1
      },
      {
        "id": "Грамотность синтаксисов программирования1",
        "from": "Базовые грамотности",
        "to": "Грамотность синтаксисов программирования",
        "value": 1
      },
      {
        "id": "Грамотность письменной и устной коммуникации1",
        "from": "Базовые грамотности",
        "to": "Грамотность письменной и устной коммуникации",
        "value": 1
      },
      {
        "id": "Грамотность онлайн взаимодействия1",
        "from": "Базовые грамотности",
        "to": "Грамотность онлайн взаимодействия",
        "value": 1
      },
      {
        "id": "Финансово-экономическая грамотность1",
        "from": "Базовые грамотности",
        "to": "Финансово-экономическая грамотность",
        "value": 1
      },
      {
        "id": "Грамотность работы с данными и информацией1",
        "from": "Базовые грамотности",
        "to": "Грамотность работы с данными и информацией",
        "value": 1
      },
      {
        "id": "Цифровое мышление1",
        "from": "Специальные компетенции",
        "to": "Цифровое мышление",
        "value": 1
      },
      {
        "id": "Работа с данными1",
        "from": "Специальные компетенции",
        "to": "Работа с данными",
        "value": 1
      },
      {
        "id": "Создание цифрового продукта1",
        "from": "Специальные компетенции",
        "to": "Создание цифрового продукта",
        "value": 1
      },
      {
        "id": "Ведение IT бизнеса1",
        "from": "Специальные компетенции",
        "to": "Ведение IT бизнеса",
        "value": 1
      },
      {
        "id": "Работа с будущим1",
        "from": "Специальные компетенции",
        "to": "Работа с будущим",
        "value": 1
      },
      {
        "id": "Универсальные компетенции1",
        "from": "Специальные компетенции",
        "to": "Универсальные компетенции",
        "value": 6
      },
      {
        "id": "Универсальные компетенции2",
        "from": "Базовые грамотности",
        "to": "Универсальные компетенции",
        "value": 5
      },
      {
        "id": "Базовые грамотности1",
        "from": "Специальные компетенции",
        "to": "Базовые грамотности",
        "value": 6
      }
    ],
    "skillToBmkEdges": [
      {
        "id": "Продажи_Ведение IT бизнеса",
        "from": "Продажи",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Организаторские способности_Взаимодействие",
        "from": "Организаторские способности",
        "to": "Взаимодействие",
        "weight": 1
      },
      {
        "id": "Ведение переговоров_Грамотность письменной и устной коммуникации",
        "from": "Ведение переговоров",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Коммуникабельность_Грамотность письменной и устной коммуникации",
        "from": "Коммуникабельность",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Работа с клиентами_Ведение IT бизнеса",
        "from": "Работа с клиентами",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Письменная коммуникация_Грамотность письменной и устной коммуникации",
        "from": "Письменная коммуникация",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Маркетинг_Ведение IT бизнеса",
        "from": "Маркетинг",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Реклама_Ведение IT бизнеса",
        "from": "Реклама",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Обучаемость_Мышление",
        "from": "Обучаемость",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Проектирование_Мышление",
        "from": "Проектирование",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Пользовательский сервис_Грамотность онлайн взаимодействия",
        "from": "Пользовательский сервис",
        "to": "Грамотность онлайн взаимодействия",
        "weight": 1
      },
      {
        "id": "Инициативность_Мышление",
        "from": "Инициативность",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Проектная деятельность_Ведение IT бизнеса",
        "from": "Проектная деятельность",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Программирование_Грамотность синтаксисов программирования",
        "from": "Программирование",
        "to": "Грамотность синтаксисов программирования",
        "weight": 1
      },
      {
        "id": "Бухгалтерский учет_Финансово-экономическая грамотность",
        "from": "Бухгалтерский учет",
        "to": "Финансово-экономическая грамотность",
        "weight": 1
      },
      {
        "id": "Управление проектами_Финансово-экономическая грамотность",
        "from": "Управление проектами",
        "to": "Финансово-экономическая грамотность",
        "weight": 1
      },
      {
        "id": "Цифровая грамотность_Цифровое мышление",
        "from": "Цифровая грамотность",
        "to": "Цифровое мышление",
        "weight": 1
      },
      {
        "id": "Маркетинг_Ведение IT бизнеса",
        "from": "Маркетинг",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Продажи_Ведение IT бизнеса",
        "from": "Продажи",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Реклама_Ведение IT бизнеса",
        "from": "Реклама",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Организаторские способности_Взаимодействие",
        "from": "Организаторские способности",
        "to": "Взаимодействие",
        "weight": 1
      },
      {
        "id": "Проектная деятельность_Ведение IT бизнеса",
        "from": "Проектная деятельность",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Ведение переговоров_Грамотность письменной и устной коммуникации",
        "from": "Ведение переговоров",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Управление проектами_Финансово-экономическая грамотность",
        "from": "Управление проектами",
        "to": "Финансово-экономическая грамотность",
        "weight": 1
      },
      {
        "id": "Программирование_Грамотность синтаксисов программирования",
        "from": "Программирование",
        "to": "Грамотность синтаксисов программирования",
        "weight": 1
      },
      {
        "id": "Проектирование_Мышление",
        "from": "Проектирование",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Маркетинговые исследования_Грамотность работы с данными и информацией",
        "from": "Маркетинговые исследования",
        "to": "Грамотность работы с данными и информацией",
        "weight": 1
      },
      {
        "id": "Коммуникабельность_Грамотность письменной и устной коммуникации",
        "from": "Коммуникабельность",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Инициативность_Мышление",
        "from": "Инициативность",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Креативность_Мышление",
        "from": "Креативность",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Работа с клиентами_Ведение IT бизнеса",
        "from": "Работа с клиентами",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Письменная коммуникация_Грамотность письменной и устной коммуникации",
        "from": "Письменная коммуникация",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Аналитическое мышление_Мышление",
        "from": "Аналитическое мышление",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Стратегическое планирование_Работа с будущим",
        "from": "Стратегическое планирование",
        "to": "Работа с будущим",
        "weight": 1
      },
      {
        "id": "Анализ данных_Работа с данными",
        "from": "Анализ данных",
        "to": "Работа с данными",
        "weight": 1
      },
      {
        "id": "Системная интеграция_Грамотность цифровых сред и интерфейсов",
        "from": "Системная интеграция",
        "to": "Грамотность цифровых сред и интерфейсов",
        "weight": 1
      },
      {
        "id": "Прогнозирование_Работа с будущим",
        "from": "Прогнозирование",
        "to": "Работа с будущим",
        "weight": 1
      },
      {
        "id": "Продажи_Ведение IT бизнеса",
        "from": "Продажи",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Ведение переговоров_Грамотность письменной и устной коммуникации",
        "from": "Ведение переговоров",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Работа с клиентами_Ведение IT бизнеса",
        "from": "Работа с клиентами",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Организаторские способности_Взаимодействие",
        "from": "Организаторские способности",
        "to": "Взаимодействие",
        "weight": 1
      },
      {
        "id": "Коммуникабельность_Грамотность письменной и устной коммуникации",
        "from": "Коммуникабельность",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Маркетинг_Ведение IT бизнеса",
        "from": "Маркетинг",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Письменная коммуникация_Грамотность письменной и устной коммуникации",
        "from": "Письменная коммуникация",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Реклама_Ведение IT бизнеса",
        "from": "Реклама",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Обучаемость_Мышление",
        "from": "Обучаемость",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Инициативность_Мышление",
        "from": "Инициативность",
        "to": "Мышление",
        "weight": 1
      },
      {
        "id": "Работа с возражениями_Грамотность письменной и устной коммуникации",
        "from": "Работа с возражениями",
        "to": "Грамотность письменной и устной коммуникации",
        "weight": 1
      },
      {
        "id": "Проектная деятельность_Ведение IT бизнеса",
        "from": "Проектная деятельность",
        "to": "Ведение IT бизнеса",
        "weight": 1
      },
      {
        "id": "Маркетинговые исследования_Грамотность работы с данными и информацией",
        "from": "Маркетинговые исследования",
        "to": "Грамотность работы с данными и информацией",
        "weight": 1
      }
    ]
};

export default sal0;
