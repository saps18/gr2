import _ from 'lodash';

import data from '../data/final';
import bmk from '../data/bmk';
import ne from '../data/ne.bak';

const START_LINKS_CUT = 100;
const MAX_SKILL_LINKS = 10;
const ROLE_VALUE_TRESHOLD = 242;
const SKILL_VALUE_TRESHOLD = 15000;

const colors: any = {
  Database: '#fff6e9',
  Frameworks: '#ffefd7',
  'Role position': '#eeffe3',
  'Operation systems': '#fffef9',
  Language: '#d2e7ff',
  IDE: '#e3eeff',
  'Version control': '#e3f0ff',
};
class GraphProvider {
  graph: Graph;

  constructor() {
    const gexf: ParsedGexf = data;
    if (gexf) this.graph = this.buildGraph(gexf);
    else
      this.graph = {
        skillNodes: [],
        roleNodes: [],
        frameworkNodes: [],
        bmkNodes: bmk.bmkNodes,
        neNodes: [],
        roleEdges: [],
        skillEdges: [],
        bmkEdges: bmk.bmkEdges,
        neEdges: [],
      };

    //// GENERATE SOME DATA FOR CONTRACTORS

    const edges = this.graph.roleEdges
      .filter(
        edge =>
          this.graph.roleNodes.map(node => node.id).includes(edge.from) &&
          this.graph.skillNodes.map(node => node.id).includes(edge.to),
      )
      .filter(
        edge =>
          edge.from !== 'Marketing_or_sales_professional' &&
          edge.from !== 'Csuite_executive' &&
          edge.from !== 'Консультант' &&
          edge.from !== 'Специалист' &&
          edge.from !== 'Эксперт' &&
          edge.to !== '1C' &&
          edge.to !== 'WindowsT' &&
          edge.to !== 'WindowsW',
      )
      .filter(edge => edge.value > 4000);

    console.log(
      JSON.stringify({
        graph: ((graph: Graph) => ({
          roleNodes: graph.roleNodes
            .filter(node => edges.map(e => e.from).includes(node.id))
            .map((node: GraphNode) => ({ id: node.id, label: node.label })),
          skillNodes: graph.skillNodes
            .filter(node => edges.map(e => e.to).includes(node.id))
            .map((node: GraphNode) => ({ id: node.id, label: node.label })),
          skillEdges: edges,
        }))(this.graph),
      }),
    );
  }

  getGraph = (): Graph => this.graph;
  getNodes = (): GraphNode[] => [
    ...this.graph.bmkNodes,
    ...this.graph.neNodes,
    ...this.graph.skillNodes,
    ...this.graph.roleNodes,
  ];
  getEdges = (): GraphEdge[] => [
    ...this.graph.bmkEdges,
    ...this.graph.neEdges,
    ...this.graph.roleEdges
      .filter(e => e.value >= ROLE_VALUE_TRESHOLD)
      .map(e => ({ ...e, type: 'roleEdge' })),
    // ...this.graph.skillEdges
    //   .filter(e => e.value >= SKILL_VALUE_TRESHOLD)
    //   .map(e => ({ ...e, type: 'skillEdge' })),
  ];

  buildGraph = (gexf: ParsedGexf): Graph => {
    const graph: Graph = {
      skillNodes: [],
      roleNodes: [],
      frameworkNodes: [],
      bmkNodes: bmk.bmkNodes,
      neNodes: [],
      roleEdges: [],
      skillEdges: [],
      bmkEdges: bmk.bmkEdges,
      neEdges: [],
    };

    // fill nodes data

    gexf.graphview.graph.nodes.node.forEach((node: GexfNode) => this.processGraphNode(graph, node));

    graph.neNodes = ne.map(entity => ({
      id: 'ne_' + entity.title,
      label: entity.title,
      hidden: false,
      group: 'ne',
      size: 10,
      shape: 'dot',
    }));

    graph.neEdges = _.flattenDeep(
      ne.map(entity => {
        return entity.bmk.map(category => ({
          id: 'ne_' + category + '_' + entity.title,
          from: category,
          to: 'ne_' + entity.title,
          value: parseInt(entity.wItRu),
        }));
      }),
    );

    // fill edges data
    gexf.graphview.graph.edges.edge.forEach((edge: GexfEdge) => this.processGraphLink(graph, edge));

    this.clearWeakLinks(graph);

    // console.log(graph);

    return graph;
  };

  // converts gexf node to graph node
  processGraphNode = (graph: Graph, node: GexfNode): void => {
    let n: GraphNode = {
      id: node._attributes.id,
      label: node._attributes.label,
      hidden: false,
      group: node.attvalues.attvalue[0]._attributes.value,
    };

    if (colors[n.group] !== undefined) {
      n.color = colors[n.group];
    }

    if (n.group === 'Role position') {
      // n.hidden = true;
      // n.physics = false;
      n.shape = 'box';
      graph.roleNodes.push(n);
    } else if (
      n.group === 'Skills1' ||
      n.group === 'Skills2' ||
      n.group === 'Sciences' ||
      n.group === 'Digital Economy Competences' ||
      n.group === 'Communication tools'
    ) {
      graph.frameworkNodes.push(n);
    } else {
      graph.skillNodes.push(n);
    }
  };

  clearWeakLinks = (graph: Graph): void => {
    // var i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // i = Math.floor(graph.skillEdges.length / 2);
    // while (i--) {
    //   graph.skillEdges.splice((i + 1) * 2 - 1, 1);
    // }
    // for (
    //   let currMaxOtherLinks = START_LINKS_CUT;
    //   currMaxOtherLinks >= MAX_SKILL_LINKS;
    //   currMaxOtherLinks--
    // ) {
    //   let allOk;
    //   let currentWeakWeight = 0;
    //   let okElements: any = [];
    //   console.log({
    //     nodes: [...graph.skillNodes, ...graph.roleNodes],
    //     edges: [
    //       ...graph.roleEdges
    //         .filter(e => e.value >= ROLE_VALUE_TRESHOLD)
    //         .map(e => ({ ...e, type: 'roleEdge' })),
    //       ...graph.skillEdges.map(e => ({ ...e, type: 'skillEdge' })),
    //     ],
    //   });
    //   do {
    //     allOk = true;
    //     currentWeakWeight =
    //       currentWeakWeight > 100
    //         ? currentWeakWeight > 105
    //           ? currentWeakWeight + 10
    //           : currentWeakWeight + 5
    //         : currentWeakWeight + 1;
    //     console.log('=======================');
    //     console.log(
    //       `MAX ${currMaxOtherLinks} LINKS. Graph has ${graph.skillEdges.length} o edgs. Clearing weight ${currentWeakWeight}`,
    //     );
    //     graph.skillNodes.forEach(node => {
    //       if (okElements[node.id]) return;
    //       const nodelinks = graph.skillEdges.filter(e => e.from === node.id || e.to === node.id);
    //       // console.log(`${node.label} has ${nodelinks.length} links`, nodelinks);
    //       if (nodelinks.length <= currMaxOtherLinks) {
    //         okElements[node.id] = true;
    //         return;
    //       }
    //       allOk = false;
    //       // console.log('Need to cut some');
    //       nodelinks.forEach(link => {
    //         const otherLabel = link.from === node.id ? link.to : link.from;
    //         const targetLinks = graph.skillEdges.filter(
    //           e => e.from === otherLabel || e.to === otherLabel,
    //         );
    //         if (link.value <= currentWeakWeight && targetLinks.length >= MAX_SKILL_LINKS / 2) {
    //           graph.skillEdges.splice(_.findIndex(graph.skillEdges, e => e.id === link.id), 1);
    //           return;
    //         }
    //       });
    //     });
    //   } while (!allOk && currentWeakWeight < 3000);
    // }
  };

  processGraphLink = (graph: Graph, edge: GexfEdge): void => {
    const e = {
      id: edge._attributes.source + '_' + edge._attributes.target,
      // name: null,
      from: edge._attributes.source,
      to: edge._attributes.target,
      // lineStyle: { normal: { color: '#66c' } },
      value: parseInt(edge._attributes.weight),
    };

    // sort alphabetically
    if (e.from > e.to) {
      let tmp = e.from;
      e.from = e.to;
      e.to = tmp;
      e.id = e.from + '_' + e.to;
    }

    const nodeFrom: GraphNode = [
      ...graph.skillNodes,
      ...graph.roleNodes,
      ...graph.frameworkNodes,
    ].filter(n => n.id === e.from)[0];
    const nodeTo: GraphNode = [
      ...graph.skillNodes,
      ...graph.roleNodes,
      ...graph.frameworkNodes,
    ].filter(n => n.id === e.to)[0];

    // role should always be a source
    if (nodeTo.group === 'Role position') {
      let tmp = e.from;
      e.from = e.to;
      e.to = tmp;
      e.id = e.from + '_' + e.to;
    }

    // if both are positions - don't link them
    // if neither is a position - push to other edges
    // if one of nodes is a position - push to roles from position to other node
    if (nodeFrom.group === 'Role position' && nodeTo.group === 'Role position') {
      // both are role positions
      return;
    } else {
      if (nodeFrom.group !== 'Role position' && nodeTo.group !== 'Role position') {
        graph.skillEdges.push(e);
      } else {
        // at least one of two nodes is a position, and it is "from" node
        graph.roleEdges.push(e);
      }
    }
  };
}

const graphProvider = new GraphProvider();
export default graphProvider;
