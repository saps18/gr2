import _ from 'lodash';

class NodeFormatter {
  tags: string[] = [];

  tagDict = ['Sber', 'WEF', 'OECD', 'UK', 'DQ', 'EU', 'GEF', 'FS', 'Coursera', 'DS', 'Stepik'];

  getUniqTags = () => {
    return _.uniq(this.tags);
  };

  fixBadData = (text: string): string => {
    return text.replace('(FS, DS)', '(FS)(DS)');
  };

  getColorByTags = (text: string): string => {
    text = this.fixBadData(text);
    const tags = this.extractTags(text);

    if (tags.length === 0) return '#000';

    switch (tags[0]) {
      case 'Sber':
        return '#50C878';
      case 'WEF':
        return '#E52B50';
      case 'OECD':
        return '#9966CC';
      case 'UK':
        return '#007FFF';
      case 'DQ':
        return '#603e9d';
      case 'EU':
        return '#FF6600';
      case 'GEF':
        return '#92000A';
      case 'FS':
        return '#008080';
      case 'Coursera':
        return '#f13744';
      case 'DS':
        return '#FF007F';
      case 'Stepik':
        return '#4a8b1f';
    }

    return '#000';
  };

  getShapeByTags = (text: string): string => {
    text = this.fixBadData(text);
    const tags = this.extractTags(text);

    const frameworks = ['Sber', 'WEF', 'OECD', 'UK', 'DQ', 'EU', 'GEF', 'FS', 'DS'];
    const courses = ['Coursera', 'Stepik'];

    this.tags = [...this.tags, ...tags];

    if (tags.length === 0) return 'circle';
    if (frameworks.includes(tags[0])) return 'rect';
    if (courses.includes(tags[0])) return 'diamond';

    return 'circle'; // just in case
  };

  getSymbolSize = (text: string, level: number): number => {
    text = this.fixBadData(text);
    const tags = this.extractTags(text);

    if (tags.length === 0) return 17;
    if (tags.length === 1) return 17;
    if (tags.length === 2) return 20;
    else return 23;

    // return 25 - level * 3; // old foumula just in case
  };

  getFontSize = (text: string, level: number): number => {
    text = this.fixBadData(text);
    const tags = this.extractTags(text);

    if (tags.length === 0) return 15;
    if (tags.length === 1) return 15;
    if (tags.length === 2) return 16;
    else return 17;

    // return 18 - level * 0.8; // old foumula just in case
  };

  extractTags = (text: string): string[] => {
    let extract = text.match(/(?<=\().+?(?=\))/g);
    if (extract) {
      extract = extract.filter(potential => this.tagDict.includes(potential));
      return extract;
    } else return [];
  };

  wrapText = (text: string, maxWidth: number): string => {
    const words = text.split(' ');
    let currentLine = '';
    let ret = '';

    for (let n = 0; n < words.length; n++) {
      let testLine = currentLine + (n > 0 ? ' ' : '') + words[n];

      if (testLine.length > maxWidth) {
        ret = ret + '\n' + currentLine;
        currentLine = words[n];
      } else {
        currentLine = testLine;
      }
    }
    ret = ret + '\n' + currentLine;
    return ret;
  };
}

const nodeFormatter = new NodeFormatter();
export default nodeFormatter;
