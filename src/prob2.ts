import graphProvider from '../services/GraphProvider';

import _ from 'lodash';
import filters from '../data/filters';
import roles from '../data/roles';
import ne from '../data/ne';
import bmk from '../data/bmk';
const vis = require('vis-network');
import n1_1 from '../data/n1'
import n2_1 from '../data/n2'

import dev0 from "../graphs/dev0";
import int0 from "../graphs/int0";
import sal0 from "../graphs/sal0";
import dev1 from "../graphs/dev1";
import int1 from "../graphs/int1";
import sal1 from "../graphs/sal1";


function removeDuplicates(originalArray:[], prop:string) {
    var newArray = [];
    var lookupObject : any = {};

    for(var i in originalArray) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
    return newArray;
}
//const nodes = graphProvider.getNodes();
//const edges = graphProvider.getEdges();

function changeGraph() {

    let selective_1 = Number((document.getElementById('sel1') as HTMLSelectElement).value);

    if ((document.getElementById('check1') as HTMLInputElement).checked)
        selective_1 = selective_1 + 3;

    let drwn = dev0;

    switch (selective_1) {
        case 1:
            drwn = dev0;
            break;
        case 2:
            drwn = sal0;
            break;
        case 3:
            drwn = int0;
            break;
        case 4:
            drwn = dev1;
            break;
        case 5:
            drwn = sal1;
            break;
        case 6:
            drwn = int1;
            break;
         }

    const Rnodes: GraphNode[] = drwn.roleNodes.map(e => ({
        ...e,
        hidden: false,
        group: 'R',
        shape: 'hexagon',
        size: 20
    }));
    const Snodes: GraphNode[] = drwn.skillNodes.map(e => ({
        ...e,
        hidden: false,
        group: 'S',
        physics: true,
        shape: 'dot'
    }));
    const Bnodes: GraphNode[] = drwn.bmkNodes.map(e => ({
        ...e,
        hidden: false,
        group: 'BMK',
        physics: true,
        shape: 'dot',
    }));

    const Sedges: GraphEdge[] = drwn.skillEdges.filter(e => e.weight >= 0.03);
    const Bedges: GraphEdge[] = drwn.bmkEdges.filter(e => e.weight >= 0.03);
    const SBedges: GraphEdge[] = drwn.skillToBmkEdges.filter(e => e.weight >= 0.03);

    let nodes : GraphNode[];
    let edges : GraphEdge[];

    if ((document.getElementById('check2') as HTMLInputElement).checked) {
        nodes = removeDuplicates([...Snodes, ...Bnodes] as [], "id");
        edges = removeDuplicates([...SBedges, ...Sedges, ...Bedges] as [], "id");
    } else {
        nodes = removeDuplicates([...Snodes, ...Rnodes] as [], "id");
        edges = removeDuplicates([...SBedges, ...Sedges, ...Bedges] as [], "id");
    }

    const nodesDataset = new vis.DataSet(nodes);
    const edgesDataset = new vis.DataSet(edges);

    const nodesView = new vis.DataView(nodesDataset);
    const edgesView = new vis.DataView(edgesDataset);

    const data = {
        nodes: nodesView,
        edges: edgesView,
        layout: {
            improvedLayout: false,
        },
        physics: {
            enabled: false,
            // timestep: 0.04,
            stabilization: {
                enabled: false,
                iterations: 10,
            },
            barnesHut: {
                gravitationalConstant: -40000,
                centralGravity: 0.0,
                springLength: 60000,
                springConstant: 0.01,
                damping: 0.09,
                avoidOverlap: 1,
            },
        },
    };

    var container = document.getElementById('mynetwork');
    var options = {};
    var network: any = new vis.Network(container, data, {});
}

window.onload = () => {
    document.getElementById('btnnn').onclick = (e: any) => {
        changeGraph();
        return false;
    }
}