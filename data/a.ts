const data: ParsedGexf = JSON.parse(`{
    "_declaration": {
        "_attributes": {
            "version": "1.0",
            "encoding": "UTF-8",
            "standalone": "no"
        }
    },
    "graphview": {
        "graph": {
            "nodes": {
                "node": [
                    {
                        "_attributes": {
                            "id": "SAP",
                            "label": "SAP",
                            "weight": "8198"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "8198"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "DelphiObject_Pascal",
                            "label": "Delphi/Object Pascal",
                            "weight": "2584"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2584"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "HTML",
                            "label": "HTML",
                            "weight": "4256"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4256"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Windows",
                            "label": "Windows",
                            "weight": "11002"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "11002"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "label": "Office / productivity suite (Microsoft Office, Google Suite, etc)",
                            "weight": "3334"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3334"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "1C",
                            "label": "1C",
                            "weight": "19934"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "19934"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "C",
                            "label": "C#",
                            "weight": "4506"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4506"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "SQL",
                            "label": "SQL",
                            "weight": "7936"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "7936"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "VBNET",
                            "label": "VB.NET",
                            "weight": "115"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "115"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MySQL",
                            "label": "MySQL",
                            "weight": "2630"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2630"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Linuxbased",
                            "label": "Linux-based",
                            "weight": "4973"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4973"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "R",
                            "label": "R",
                            "weight": "1336"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1336"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "System_administrator",
                            "label": "System administrator",
                            "weight": "3754"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3754"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Fortran",
                            "label": "Fortran",
                            "weight": "181"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "181"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Data_or_business_analyst",
                            "label": "Data or business analyst",
                            "weight": "880"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "880"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_BigQuery",
                            "label": "Google BigQuery",
                            "weight": "62"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "62"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_Cloud_Storage",
                            "label": "Google Cloud Storage",
                            "weight": "62"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "62"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_HangoutsChat",
                            "label": "Google Hangouts/Chat",
                            "weight": "62"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "62"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "label": "Other wiki tool (Github, Google Sites, proprietary software, etc)",
                            "weight": "211"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "211"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Educator_or_academic_researcher",
                            "label": "Educator or academic researcher",
                            "weight": "1550"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1550"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Marketing_or_sales_professional",
                            "label": "Marketing or sales professional",
                            "weight": "2769"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2769"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Matlab",
                            "label": "Matlab",
                            "weight": "374"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "374"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "QA_or_test_developer",
                            "label": "QA or test developer",
                            "weight": "3254"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3254"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Assembly",
                            "label": "Assembly",
                            "weight": "127"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "127"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "CSS",
                            "label": "CSS",
                            "weight": "2797"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2797"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Designer",
                            "label": "Designer",
                            "weight": "1267"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1267"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Database_Administrator",
                            "label": "Database Administrator",
                            "weight": "800"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "800"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Mobile_developer",
                            "label": "Mobile developer",
                            "weight": "146"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "146"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MSSQL",
                            "label": "MSSQL",
                            "weight": "709"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "709"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Pascal",
                            "label": "Pascal",
                            "weight": "998"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "998"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PostgreSQL",
                            "label": "PostgreSQL",
                            "weight": "661"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "661"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Basic",
                            "label": "Visual Basic",
                            "weight": "658"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "658"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Java",
                            "label": "Java",
                            "weight": "1938"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1938"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cobol",
                            "label": "Cobol",
                            "weight": "19"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "19"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ObjectiveC",
                            "label": "Objective-C",
                            "weight": "62"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "62"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "iOS",
                            "label": "iOS",
                            "weight": "306"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "306"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "BashShell",
                            "label": "Bash/Shell",
                            "weight": "368"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "368"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Go",
                            "label": "Go",
                            "weight": "48"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "48"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Python",
                            "label": "Python",
                            "weight": "717"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "717"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Eclipse",
                            "label": "Eclipse",
                            "weight": "162"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "162"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IntelliJ",
                            "label": "IntelliJ",
                            "weight": "141"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "141"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NetBeans",
                            "label": "NetBeans",
                            "weight": "106"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "106"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PHPStorm",
                            "label": "PHPStorm",
                            "weight": "34"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "34"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Zend",
                            "label": "Zend",
                            "weight": "39"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "39"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Memcached",
                            "label": "Memcached",
                            "weight": "25"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "25"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Redis",
                            "label": "Redis",
                            "weight": "32"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "32"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Angular",
                            "label": "Angular",
                            "weight": "70"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "70"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Confluence",
                            "label": "Confluence",
                            "weight": "86"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "86"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Jira",
                            "label": "Jira",
                            "weight": "453"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "453"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Trello",
                            "label": "Trello",
                            "weight": "45"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "45"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Git",
                            "label": "Git",
                            "weight": "773"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "773"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Mercurial",
                            "label": "Mercurial",
                            "weight": "79"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "79"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Csuite_executive_CEO_CTO_etc",
                            "label": "C-suite executive (CEO, CTO, etc)",
                            "weight": "477"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "477"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "label": "Microsoft Azure (Tables, CosmosDB, SQL, etc)",
                            "weight": "411"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "411"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "BSDUnix",
                            "label": "BSD/Unix",
                            "weight": "897"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "897"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Ruby",
                            "label": "Ruby",
                            "weight": "92"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "92"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Studio",
                            "label": "Visual Studio",
                            "weight": "744"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "744"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Xamarin",
                            "label": "Xamarin",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Android",
                            "label": "Android",
                            "weight": "650"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "650"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Perl",
                            "label": "Perl",
                            "weight": "277"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "277"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ABAP",
                            "label": "ABAP",
                            "weight": "61"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "61"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "SQLite",
                            "label": "SQLite",
                            "weight": "212"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "212"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Facebook",
                            "label": "Facebook",
                            "weight": "516"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "516"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Slack",
                            "label": "Slack",
                            "weight": "18"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "18"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MacOs",
                            "label": "MacOs",
                            "weight": "218"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "218"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NET_Core",
                            "label": ".NET Core",
                            "weight": "100"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "100"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IBM_DB2",
                            "label": "IBM DB2",
                            "weight": "173"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "173"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Studio_Code",
                            "label": "Visual Studio Code",
                            "weight": "97"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "97"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Scala",
                            "label": "Scala",
                            "weight": "70"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "70"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Swift",
                            "label": "Swift",
                            "weight": "205"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "205"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Team_Foundation_Version_Control",
                            "label": "Team Foundation Version Control",
                            "weight": "46"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "46"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Android_Studio",
                            "label": "Android Studio",
                            "weight": "163"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "163"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Django",
                            "label": "Django",
                            "weight": "96"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "96"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PyCharm",
                            "label": "PyCharm",
                            "weight": "24"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "24"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Spark",
                            "label": "Spark",
                            "weight": "49"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "49"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Spring",
                            "label": "Spring",
                            "weight": "100"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "100"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TypeScript",
                            "label": "TypeScript",
                            "weight": "29"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "29"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Apache_Hbase",
                            "label": "Apache Hbase",
                            "weight": "23"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "23"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Apache_Hive",
                            "label": "Apache Hive",
                            "weight": "23"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "23"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Kotlin",
                            "label": "Kotlin",
                            "weight": "34"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "34"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NodeJS",
                            "label": "NodeJS",
                            "weight": "34"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "34"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Xcode",
                            "label": "Xcode",
                            "weight": "23"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "23"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "React",
                            "label": "React",
                            "weight": "106"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "106"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TensorFlow",
                            "label": "TensorFlow",
                            "weight": "12"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "12"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ElasticSearch",
                            "label": "ElasticSearch",
                            "weight": "10"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "10"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Clojure",
                            "label": "Clojure",
                            "weight": "2"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cordova",
                            "label": "Cordova",
                            "weight": "13"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "13"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Other_chat_system_IRC_proprietary_software_etc",
                            "label": "Other chat system (IRC, proprietary software, etc)",
                            "weight": "64"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "64"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Sublime_Text",
                            "label": "Sublime Text",
                            "weight": "25"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "25"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TorchPyTorch",
                            "label": "Torch/PyTorch",
                            "weight": "3"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Frontend_developer",
                            "label": "Front-end developer",
                            "weight": "38"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "38"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MariaDB",
                            "label": "MariaDB",
                            "weight": "18"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "18"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Emacs",
                            "label": "Emacs",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "F",
                            "label": "F#",
                            "weight": "86"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "86"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Notepad",
                            "label": "Notepad++",
                            "weight": "34"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "34"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Subversion",
                            "label": "Subversion",
                            "weight": "30"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "30"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Vim",
                            "label": "Vim",
                            "weight": "13"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "13"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Atom",
                            "label": "Atom",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Groovy",
                            "label": "Groovy",
                            "weight": "5"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "5"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Lua",
                            "label": "Lua",
                            "weight": "29"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "29"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Hadoop",
                            "label": "Hadoop",
                            "weight": "41"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "41"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Desktop_or_enterprise_applications_developer",
                            "label": "Desktop or enterprise applications developer",
                            "weight": "32"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "32"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Hack",
                            "label": "Hack",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cassandra",
                            "label": "Cassandra",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Fullstack_developer",
                            "label": "Full-stack developer",
                            "weight": "5"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "5"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Data_scientist_or_machine_learning_specialist",
                            "label": "Data scientist or machine learning specialist",
                            "weight": "11"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "11"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "RStudio",
                            "label": "RStudio",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Game_or_graphics_developer",
                            "label": "Game or graphics developer",
                            "weight": "11"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "11"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Engineering_manager",
                            "label": "Engineering manager",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Amazon_DynamoDB",
                            "label": "Amazon DynamoDB",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Julia",
                            "label": "Julia",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "HipChat",
                            "label": "HipChat",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Stack_Overflow_Enterprise",
                            "label": "Stack Overflow Enterprise",
                            "weight": "2"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Haskell",
                            "label": "Haskell",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Coda",
                            "label": "Coda",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IPythonJupiter",
                            "label": "IPython/Jupiter",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "DevOps_specialist",
                            "label": "DevOps specialist",
                            "weight": "2"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Erlang",
                            "label": "Erlang",
                            "weight": "4"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    }
                ]
            },
            "edges": {
                "edge": [
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "HTML",
                            "weight": "514"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Windows",
                            "weight": "757"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Windows",
                            "weight": "1155"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "DelphiObject_Pascal",
                            "weight": "866"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SQL",
                            "weight": "1757"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "VBNET",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MySQL",
                            "weight": "639"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Linuxbased",
                            "weight": "801"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Windows",
                            "weight": "1201"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SQL",
                            "weight": "1124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "VBNET",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MySQL",
                            "weight": "404"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Linuxbased",
                            "weight": "401"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "VBNET",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MySQL",
                            "weight": "1003"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Linuxbased",
                            "weight": "1432"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Windows",
                            "weight": "2957"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "MySQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Linuxbased",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Windows",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Linuxbased",
                            "weight": "814"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Windows",
                            "weight": "923"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Linuxbased",
                            "target": "Windows",
                            "weight": "3559"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SAP",
                            "weight": "757"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Windows",
                            "weight": "1006"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "SAP",
                            "weight": "3229"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Windows",
                            "weight": "1826"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "708"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Google_Cloud_Storage",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "1C",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Google_HangoutsChat",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "1C",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Google_HangoutsChat",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Google_HangoutsChat",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1507"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Marketing_or_sales_professional",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "1C",
                            "weight": "493"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SQL",
                            "weight": "1245"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "1C",
                            "weight": "1263"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "1C",
                            "weight": "3157"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Windows",
                            "weight": "4264"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Linuxbased",
                            "weight": "1635"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "R",
                            "weight": "115"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "QA_or_test_developer",
                            "weight": "215"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SAP",
                            "weight": "235"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "313"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "QA_or_test_developer",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "103"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "SAP",
                            "weight": "212"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "SQL",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "1C",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "SAP",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "SAP",
                            "weight": "253"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "HTML",
                            "weight": "2586"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "1C",
                            "weight": "157"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "SAP",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "QA_or_test_developer",
                            "weight": "247"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Windows",
                            "weight": "594"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "1C",
                            "weight": "964"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "1C",
                            "weight": "747"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "1C",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "CSS",
                            "weight": "601"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "HTML",
                            "weight": "953"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Database_Administrator",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SQL",
                            "weight": "778"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Database_Administrator",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "QA_or_test_developer",
                            "weight": "94"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Windows",
                            "weight": "606"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Database_Administrator",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "QA_or_test_developer",
                            "weight": "162"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Database_Administrator",
                            "weight": "159"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "QA_or_test_developer",
                            "weight": "427"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "QA_or_test_developer",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Windows",
                            "weight": "183"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Windows",
                            "weight": "384"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MSSQL",
                            "weight": "137"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Windows",
                            "weight": "387"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "1C",
                            "weight": "199"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Pascal",
                            "weight": "500"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SQL",
                            "weight": "382"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "System_administrator",
                            "weight": "136"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "1C",
                            "weight": "1485"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "PostgreSQL",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "1C",
                            "weight": "323"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "1C",
                            "weight": "198"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Windows",
                            "weight": "298"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "DelphiObject_Pascal",
                            "weight": "268"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MSSQL",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MySQL",
                            "weight": "946"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MSSQL",
                            "weight": "124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MSSQL",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MySQL",
                            "weight": "1122"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "MySQL",
                            "weight": "250"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Basic",
                            "weight": "315"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "QA_or_test_developer",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Windows",
                            "weight": "230"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Linuxbased",
                            "weight": "742"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Java",
                            "weight": "773"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SQL",
                            "weight": "796"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Cobol",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Fortran",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Educator_or_academic_researcher",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Fortran",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Java",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "SQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Educator_or_academic_researcher",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "1C",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Java",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "SQL",
                            "weight": "69"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Educator_or_academic_researcher",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "System_administrator",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "1C",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Linuxbased",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Windows",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Educator_or_academic_researcher",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "System_administrator",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "1C",
                            "weight": "329"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Linuxbased",
                            "weight": "403"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Windows",
                            "weight": "413"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "System_administrator",
                            "weight": "875"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "System_administrator",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "1C",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Linuxbased",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Windows",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "ObjectiveC",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Pascal",
                            "weight": "442"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "ObjectiveC",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SAP",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Pascal",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SQL",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "1C",
                            "weight": "337"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SAP",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Windows",
                            "weight": "370"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "iOS",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "iOS",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "iOS",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "iOS",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Visual_Basic",
                            "weight": "230"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "MSSQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "C",
                            "weight": "124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "CSS",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "DelphiObject_Pascal",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Go",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "HTML",
                            "weight": "88"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Pascal",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Python",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Eclipse",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "IntelliJ",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NetBeans",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PHPStorm",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Zend",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MSSQL",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MySQL",
                            "weight": "150"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PostgreSQL",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Redis",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Angular",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Confluence",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Jira",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Git",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Mercurial",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Linuxbased",
                            "weight": "268"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Go",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Python",
                            "weight": "382"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Eclipse",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "IntelliJ",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NetBeans",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PHPStorm",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Memcached",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MSSQL",
                            "weight": "156"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PostgreSQL",
                            "weight": "208"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Redis",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Angular",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Confluence",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Jira",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Git",
                            "weight": "250"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Mercurial",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Go",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Pascal",
                            "weight": "145"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Python",
                            "weight": "184"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Eclipse",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "IntelliJ",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "NetBeans",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PHPStorm",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Zend",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PostgreSQL",
                            "weight": "134"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Redis",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Angular",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Confluence",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Jira",
                            "weight": "74"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Trello",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Git",
                            "weight": "311"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Mercurial",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Linuxbased",
                            "weight": "457"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Go",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Python",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Eclipse",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "IntelliJ",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "NetBeans",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PHPStorm",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Zend",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PostgreSQL",
                            "weight": "124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Redis",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Angular",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Confluence",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Jira",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Git",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Mercurial",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "HTML",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Pascal",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Python",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Eclipse",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "IntelliJ",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NetBeans",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "PHPStorm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Zend",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "MSSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "MySQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "PostgreSQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Redis",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Angular",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Git",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Mercurial",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Linuxbased",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Pascal",
                            "weight": "304"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Python",
                            "weight": "230"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Eclipse",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "IntelliJ",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "NetBeans",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PHPStorm",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Zend",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Memcached",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PostgreSQL",
                            "weight": "193"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Redis",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Angular",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Confluence",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Jira",
                            "weight": "90"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Trello",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Git",
                            "weight": "303"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Mercurial",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Linuxbased",
                            "weight": "743"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Python",
                            "weight": "69"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Eclipse",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "IntelliJ",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NetBeans",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PHPStorm",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Zend",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MSSQL",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MySQL",
                            "weight": "173"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PostgreSQL",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Confluence",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Jira",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Git",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Mercurial",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Linuxbased",
                            "weight": "197"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Eclipse",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "IntelliJ",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NetBeans",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PHPStorm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Zend",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MSSQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MySQL",
                            "weight": "184"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PostgreSQL",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Redis",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Confluence",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Jira",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Git",
                            "weight": "102"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Mercurial",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Linuxbased",
                            "weight": "272"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "IntelliJ",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "NetBeans",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PHPStorm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Zend",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "MSSQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "MySQL",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PostgreSQL",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Angular",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Confluence",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Jira",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Git",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Mercurial",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Linuxbased",
                            "weight": "74"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "NetBeans",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PHPStorm",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Zend",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "MSSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "MySQL",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PostgreSQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Confluence",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Jira",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Git",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Mercurial",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Linuxbased",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PHPStorm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Zend",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "MSSQL",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "MySQL",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PostgreSQL",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Git",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Mercurial",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Linuxbased",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Zend",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "MSSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "MySQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "PostgreSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Angular",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Jira",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Git",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Mercurial",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Linuxbased",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MSSQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MySQL",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Angular",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Git",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Mercurial",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "MSSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "MySQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "PostgreSQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Redis",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Trello",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Mercurial",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Linuxbased",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Confluence",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Git",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Mercurial",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Linuxbased",
                            "weight": "210"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "PostgreSQL",
                            "weight": "305"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Redis",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Angular",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Confluence",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Jira",
                            "weight": "93"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Git",
                            "weight": "263"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Mercurial",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Redis",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Confluence",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Jira",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Git",
                            "weight": "97"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Mercurial",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Linuxbased",
                            "weight": "296"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Angular",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Confluence",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Jira",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Git",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Linuxbased",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Confluence",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Jira",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Trello",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Git",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Mercurial",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Jira",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Git",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Linuxbased",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Trello",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Git",
                            "weight": "114"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Mercurial",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Linuxbased",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Mercurial",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Linuxbased",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Mercurial",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Linuxbased",
                            "weight": "262"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Linuxbased",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Basic",
                            "weight": "245"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Basic",
                            "weight": "147"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Visual_Basic",
                            "weight": "87"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Git",
                            "weight": "292"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Git",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Windows",
                            "weight": "131"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "477"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "187"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "431"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "328"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "C",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Windows",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Java",
                            "weight": "319"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Pascal",
                            "weight": "133"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NetBeans",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NetBeans",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Windows",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Database_Administrator",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "BSDUnix",
                            "weight": "332"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "Windows",
                            "weight": "599"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "QA_or_test_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Git",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Linuxbased",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Git",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Linuxbased",
                            "weight": "213"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Linuxbased",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "System_administrator",
                            "weight": "117"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "System_administrator",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "1C",
                            "weight": "681"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Linuxbased",
                            "weight": "104"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Fortran",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Visual_Basic",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Studio",
                            "weight": "479"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Studio",
                            "weight": "241"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Visual_Studio",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Database_Administrator",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Studio",
                            "weight": "451"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Visual_Studio",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Database_Administrator",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "1C",
                            "weight": "215"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Database_Administrator",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "1C",
                            "weight": "175"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Linuxbased",
                            "weight": "169"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Windows",
                            "weight": "247"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "1C",
                            "weight": "408"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "1C",
                            "weight": "896"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "R",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Linuxbased",
                            "weight": "168"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SQL",
                            "weight": "329"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "System_administrator",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "1C",
                            "weight": "97"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Windows",
                            "weight": "239"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "System_administrator",
                            "weight": "151"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Xamarin",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PostgreSQL",
                            "weight": "295"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Jira",
                            "weight": "194"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "1C",
                            "weight": "774"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Android",
                            "weight": "141"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "BSDUnix",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "iOS",
                            "weight": "183"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "Linuxbased",
                            "weight": "214"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "iOS",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "Linuxbased",
                            "weight": "458"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "Linuxbased",
                            "weight": "117"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Linuxbased",
                            "weight": "350"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "System_administrator",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "SAP",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "QA_or_test_developer",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Windows",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "System_administrator",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "System_administrator",
                            "weight": "160"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "SAP",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Visual_Basic",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "SAP",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Jira",
                            "weight": "82"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "QA_or_test_developer",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "System_administrator",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "System_administrator",
                            "weight": "139"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "System_administrator",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "365"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "1C",
                            "weight": "173"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Data_or_business_analyst",
                            "weight": "89"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Data_or_business_analyst",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "System_administrator",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Linuxbased",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Windows",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "1C",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Windows",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "R",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Jira",
                            "weight": "147"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "74"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "System_administrator",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "1C",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Linuxbased",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Windows",
                            "weight": "82"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "QA_or_test_developer",
                            "weight": "122"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "SAP",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "SAP",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Linuxbased",
                            "weight": "161"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "QA_or_test_developer",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Data_or_business_analyst",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Perl",
                            "weight": "106"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SQL",
                            "weight": "127"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MySQL",
                            "weight": "151"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "1C",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Windows",
                            "weight": "131"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "BSDUnix",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "BSDUnix",
                            "weight": "153"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "BSDUnix",
                            "weight": "338"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Java",
                            "weight": "482"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SQLite",
                            "weight": "82"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Java",
                            "weight": "689"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SQLite",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MySQL",
                            "weight": "442"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SQLite",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "SQLite",
                            "weight": "117"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "SQLite",
                            "weight": "134"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Confluence",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Confluence",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Confluence",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "201"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "BSDUnix",
                            "weight": "122"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "1C",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Slack",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Slack",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Slack",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Slack",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Slack",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Facebook",
                            "weight": "204"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Facebook",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Designer",
                            "weight": "101"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Designer",
                            "weight": "145"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Facebook",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Facebook",
                            "weight": "74"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Facebook",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "SAP",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "SAP",
                            "weight": "75"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Marketing_or_sales_professional",
                            "weight": "138"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "MySQL",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Designer",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "QA_or_test_developer",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SAP",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "MacOs",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "MacOs",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "MacOs",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Linuxbased",
                            "target": "MacOs",
                            "weight": "118"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MacOs",
                            "target": "Windows",
                            "weight": "192"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Visual_Studio",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NET_Core",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "NET_Core",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "NET_Core",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Studio",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "BSDUnix",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "BSDUnix",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "IBM_DB2",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "IBM_DB2",
                            "weight": "76"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "MySQL",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Database_Administrator",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Database_Administrator",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "1C",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "QA_or_test_developer",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Jira",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Windows",
                            "weight": "291"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Studio_Code",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "BSDUnix",
                            "weight": "165"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MacOs",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Visual_Studio",
                            "weight": "135"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Visual_Studio_Code",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "BSDUnix",
                            "weight": "84"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MacOs",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Basic",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Studio",
                            "weight": "154"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Studio_Code",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "BSDUnix",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MacOs",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Visual_Studio_Code",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "BSDUnix",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MacOs",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Visual_Studio_Code",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "BSDUnix",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Visual_Studio_Code",
                            "weight": "84"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "BSDUnix",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Linuxbased",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Windows",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "BSDUnix",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Linuxbased",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Windows",
                            "weight": "105"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "MacOs",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Data_or_business_analyst",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SAP",
                            "weight": "86"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "1C",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "SAP",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Matlab",
                            "weight": "163"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SQLite",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Matlab",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "R",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SQLite",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Matlab",
                            "weight": "82"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Python",
                            "weight": "191"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "R",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Python",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "R",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SQL",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "MySQL",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SQLite",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Linuxbased",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Windows",
                            "weight": "67"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "R",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SQLite",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SQL",
                            "weight": "165"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MySQL",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SQLite",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Linuxbased",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Windows",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "SAP",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "90"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Team_Foundation_Version_Control",
                            "target": "Linuxbased",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Team_Foundation_Version_Control",
                            "target": "Windows",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "BSDUnix",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "BSDUnix",
                            "weight": "168"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Marketing_or_sales_professional",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Android_Studio",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Android",
                            "weight": "189"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Android_Studio",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Android",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Android_Studio",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Android",
                            "weight": "325"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Visual_Studio",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "MySQL",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "1C",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Android",
                            "weight": "163"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Linuxbased",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Windows",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MySQL",
                            "weight": "117"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Android",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Android",
                            "weight": "119"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "Windows",
                            "weight": "248"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Perl",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "PostgreSQL",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Pascal",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Confluence",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Slack",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Trello",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Slack",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Slack",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Slack",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Linuxbased",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Educator_or_academic_researcher",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Scala",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "SQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Linuxbased",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "MacOs",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MacOs",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "MacOs",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "1C",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "BSDUnix",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Windows",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Java",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SQL",
                            "weight": "158"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "138"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Git",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Database_Administrator",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "SAP",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Perl",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "1C",
                            "weight": "101"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Windows",
                            "weight": "190"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Perl",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Perl",
                            "weight": "95"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Linuxbased",
                            "weight": "135"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "System_administrator",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "BSDUnix",
                            "weight": "88"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Perl",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "BSDUnix",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "iOS",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "BSDUnix",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "BSDUnix",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "BSDUnix",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Python",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Matlab",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Matlab",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Matlab",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Visual_Basic",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Visual_Studio",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Django",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Django",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Django",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SQLite",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Django",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Django",
                            "weight": "87"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Django",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "SQLite",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Django",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "1C",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Django",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "BSDUnix",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Django",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "BSDUnix",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Windows",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Studio",
                            "weight": "133"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "QA_or_test_developer",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Django",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Django",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Jira",
                            "weight": "85"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Git",
                            "weight": "300"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NET_Core",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Pascal",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PyCharm",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PyCharm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Visual_Studio",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "SQLite",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Visual_Studio",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "MSSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "MySQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "SQLite",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "SQLite",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "SQLite",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "System_administrator",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "System_administrator",
                            "weight": "103"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Android",
                            "weight": "124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Android",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "QA_or_test_developer",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "PostgreSQL",
                            "weight": "150"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "PostgreSQL",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "MySQL",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "Windows",
                            "weight": "151"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "1C",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Visual_Studio",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Git",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Git",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Jira",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "DelphiObject_Pascal",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Designer",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Designer",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Swift",
                            "weight": "85"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Spark",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "MySQL",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Spark",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Linuxbased",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Spark",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Spark",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Spark",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Linuxbased",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Windows",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Android",
                            "weight": "206"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Android",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "iOS",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "PostgreSQL",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "SAP",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Spark",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "QA_or_test_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Android",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Android",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Spring",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Spring",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Spring",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Spring",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Spring",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Git",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "PostgreSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Designer",
                            "weight": "117"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "System_administrator",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Windows",
                            "weight": "116"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "BSDUnix",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Redis",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Android",
                            "weight": "76"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MSSQL",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "IntelliJ",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "IntelliJ",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Spring",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Spring",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Linuxbased",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TypeScript",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Apache_Hbase",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Apache_Hive",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Apache_Hbase",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Apache_Hive",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Apache_Hive",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Basic",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Windows",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Android",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Android",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Kotlin",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Kotlin",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "SQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Git",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Android",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Git",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Android_Studio",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Git",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "iOS",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "iOS",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Ruby",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NodeJS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Ruby",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Android_Studio",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "NodeJS",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Ruby",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Android_Studio",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "NodeJS",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Ruby",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Eclipse",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Angular",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Django",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NodeJS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Ruby",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Android_Studio",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Android",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Android_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Eclipse",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MySQL",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "PostgreSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SQLite",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "1C",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Eclipse",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "SQLite",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Git",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "SQLite",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "1C",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Android",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "NodeJS",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Android",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Git",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Angular",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Git",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Linuxbased",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Git",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Linuxbased",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Marketing_or_sales_professional",
                            "weight": "87"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Zend",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Marketing_or_sales_professional",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Marketing_or_sales_professional",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Xcode",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_BigQuery",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_Cloud_Storage",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_BigQuery",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_Cloud_Storage",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_BigQuery",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_Cloud_Storage",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Linuxbased",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Git",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Linuxbased",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TensorFlow",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "TensorFlow",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Git",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Linuxbased",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "1C",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "IntelliJ",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "NetBeans",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "SQLite",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Android",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Windows",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Android",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "QA_or_test_developer",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Kotlin",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Scala",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Scala",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "ElasticSearch",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Mercurial",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Python",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Scala",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Redis",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Jira",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Scala",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "PostgreSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Redis",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "ElasticSearch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Redis",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Mercurial",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Redis",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Git",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "BSDUnix",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "BSDUnix",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "BSDUnix",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "Java",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Eclipse",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Visual_Studio",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Windows",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Spring",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Spring",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Android",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Swift",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "React",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Spring",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "ObjectiveC",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Swift",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "React",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "iOS",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "ObjectiveC",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Swift",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "React",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "iOS",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Swift",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "MySQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Android",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "iOS",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "React",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "React",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Spring",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Android",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "iOS",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Android_Studio",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Android_Studio",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "QA_or_test_developer",
                            "weight": "137"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Android",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "iOS",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "iOS",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Kotlin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Swift",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Spring",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "ObjectiveC",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Pascal",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Swift",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "SQLite",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Spring",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SQLite",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Swift",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "SQLite",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Database_Administrator",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Database_Administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Android",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Cordova",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Cordova",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Angular",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Kotlin",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Kotlin",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Visual_Studio_Code",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Android",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Git",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Redis",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "SQLite",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Facebook",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Facebook",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Facebook",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Facebook",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "React",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Database_Administrator",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Android",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "QA_or_test_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Mobile_developer",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Mobile_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Swift",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Mobile_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Mobile_developer",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Mobile_developer",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Mobile_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "QA_or_test_developer",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "QA_or_test_developer",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Xcode",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "1C",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Visual_Studio",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "IntelliJ",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "iOS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Linuxbased",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "1C",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "1C",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Matlab",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MacOs",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MacOs",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "BSDUnix",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Designer",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Git",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Designer",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Windows",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Basic",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "SQLite",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "ObjectiveC",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "QA_or_test_developer",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Android",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "BSDUnix",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "BSDUnix",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Android",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "VBNET",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "NET_Core",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "VBNET",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NET_Core",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "BSDUnix",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Windows",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "Windows",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Linuxbased",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "SAP",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Android",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "QA_or_test_developer",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "SAP",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Marketing_or_sales_professional",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "System_administrator",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Studio_Code",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Visual_Basic",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Slack",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Marketing_or_sales_professional",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "R",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "89"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "QA_or_test_developer",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Visual_Basic",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Educator_or_academic_researcher",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Marketing_or_sales_professional",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Android",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "iOS",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Android",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "iOS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Data_or_business_analyst",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "ObjectiveC",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Java",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Matlab",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Visual_Studio",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "MySQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Linuxbased",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Designer",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Designer",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Designer",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Designer",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TensorFlow",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Xamarin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "ObjectiveC",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "TensorFlow",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Xamarin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Xamarin",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "PostgreSQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Python",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "BSDUnix",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "React",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "TensorFlow",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TensorFlow",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Xamarin",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "TensorFlow",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Xamarin",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "React",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "iOS",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Xamarin",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "TorchPyTorch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "HTML",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "VBNET",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Visual_Studio",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "VBNET",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "1C",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "SAP",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "R",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Pascal",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SQL",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SAP",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Android_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Android_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "iOS",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MSSQL",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "System_administrator",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "QA_or_test_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "iOS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Windows",
                            "weight": "89"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MariaDB",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "MSSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "MySQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "PostgreSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Windows",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Emacs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Educator_or_academic_researcher",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Emacs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "BSDUnix",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SAP",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "IntelliJ",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "SAP",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Jira",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Database_Administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Database_Administrator",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Git",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "F",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "F",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "SQL",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "MySQL",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "SQLite",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Scala",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "SAP",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "SAP",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PyCharm",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PyCharm",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Django",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PyCharm",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PyCharm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PyCharm",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PyCharm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "1C",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Marketing_or_sales_professional",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "1C",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "QA_or_test_developer",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SAP",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Perl",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SAP",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Visual_Studio",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "QA_or_test_developer",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SAP",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SAP",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MacOs",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Educator_or_academic_researcher",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Facebook",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Facebook",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "IBM_DB2",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "PostgreSQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "IBM_DB2",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "PostgreSQL",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "PostgreSQL",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "1C",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "SAP",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Linuxbased",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Windows",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "MacOs",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "NetBeans",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Git",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Mercurial",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Windows",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Windows",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Notepad",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Notepad",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Confluence",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Confluence",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Subversion",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Subversion",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Zend",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Subversion",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Subversion",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Subversion",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "SQLite",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "1C",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Windows",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Subversion",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Mercurial",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Subversion",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Mercurial",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Subversion",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Subversion",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Subversion",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Subversion",
                            "target": "Windows",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Emacs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Vim",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Emacs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Vim",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Vim",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Emacs",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Linuxbased",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "React",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Facebook",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Linuxbased",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Linuxbased",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Google_BigQuery",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Google_Cloud_Storage",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Google_HangoutsChat",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Notepad",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "MySQL",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Subversion",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Subversion",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Subversion",
                            "target": "Linuxbased",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Visual_Basic",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Visual_Studio",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Visual_Studio_Code",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Eclipse",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Frontend_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Frontend_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Angular",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "React",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "React",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TypeScript",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Marketing_or_sales_professional",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "React",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Atom",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Atom",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Studio_Code",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Studio_Code",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "MySQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Sublime_Text",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Sublime_Text",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Sublime_Text",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Visual_Studio",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Visual_Studio_Code",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Windows",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Visual_Studio",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Visual_Studio_Code",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "SQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "TypeScript",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "MSSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "MSSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "MySQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "PostgreSQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Mercurial",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MSSQL",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Sublime_Text",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Sublime_Text",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Visual_Studio",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Designer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "System_administrator",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "System_administrator",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "iOS",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Windows",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Lua",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Hadoop",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Lua",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Lua",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Lua",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Hadoop",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "MSSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Windows",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Hadoop",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Hadoop",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Windows",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "R",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Android",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "MacOs",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Android",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "iOS",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MariaDB",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MariaDB",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "IBM_DB2",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "IBM_DB2",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "IBM_DB2",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Pascal",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "R",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "1C",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Hadoop",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Jira",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Linuxbased",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Windows",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Hadoop",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Hadoop",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Jira",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Hadoop",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Hadoop",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Jira",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Linuxbased",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "IBM_DB2",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Spring",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Spring",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Android",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Windows",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Atom",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Atom",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Notepad",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Atom",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Notepad",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Eclipse",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "IntelliJ",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Notepad",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Notepad",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Notepad",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Visual_Basic",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "QA_or_test_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Notepad",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Google_BigQuery",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Google_Cloud_Storage",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Google_HangoutsChat",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Google_HangoutsChat",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "BSDUnix",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Sublime_Text",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Vim",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Sublime_Text",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Vim",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Sublime_Text",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Vim",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Vim",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "IBM_DB2",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Mobile_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "IBM_DB2",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "IBM_DB2",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "BSDUnix",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Perl",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SQLite",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SQLite",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MacOs",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Educator_or_academic_researcher",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "iOS",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "1C",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Database_Administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "BSDUnix",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "VBNET",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Windows",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "QA_or_test_developer",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Visual_Studio",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "System_administrator",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Mobile_developer",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Mercurial",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Database_Administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "ElasticSearch",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "1C",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Angular",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Trello",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Swift",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Xcode",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Xcode",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Perl",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Visual_Studio",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Xcode",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "NET_Core",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "iOS",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Java",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Atom",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hack",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hack",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hack",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hack",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hack",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Android_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Notepad",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Android",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Android_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "NetBeans",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Notepad",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "SAP",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "1C",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "SQL",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "VBNET",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Cassandra",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Spring",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Mercurial",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Cassandra",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Lua",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Java",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Lua",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Scala",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Python",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Scala",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "SQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Linuxbased",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NodeJS",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Educator_or_academic_researcher",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Fullstack_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Fullstack_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "Cordova",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Git",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Marketing_or_sales_professional",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Marketing_or_sales_professional",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Marketing_or_sales_professional",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Data_or_business_analyst",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Data_or_business_analyst",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "MacOs",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Confluence",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Jira",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Subversion",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Facebook",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "iOS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TypeScript",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "TypeScript",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Visual_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MariaDB",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NET_Core",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Perl",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NET_Core",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NET_Core",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "NET_Core",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NET_Core",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "NET_Core",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Angular",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Educator_or_academic_researcher",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Studio_Code",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Scala",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Scala",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "MSSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "NET_Core",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "RStudio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "RStudio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "RStudio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "RStudio",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Cordova",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Angular",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Cordova",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Cordova",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "IBM_DB2",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "System_administrator",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "MySQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "QA_or_test_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "iOS",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "PostgreSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Pascal",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Perl",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "IBM_DB2",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Visual_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "1C",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Windows",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "VBNET",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "MacOs",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Apache_Hbase",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Apache_Hive",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Apache_Hbase",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Apache_Hive",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Apache_Hbase",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Apache_Hive",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Zend",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Swift",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Swift",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MacOs",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "MacOs",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "MacOs",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Marketing_or_sales_professional",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "MSSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Data_or_business_analyst",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Data_or_business_analyst",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Data_or_business_analyst",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Jira",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Game_or_graphics_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Game_or_graphics_developer",
                            "target": "BSDUnix",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Game_or_graphics_developer",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "BSDUnix",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Educator_or_academic_researcher",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Educator_or_academic_researcher",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Ruby",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Ruby",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Memcached",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "iOS",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "BSDUnix",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Educator_or_academic_researcher",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Visual_Studio_Code",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "SQLite",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Vim",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Vim",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "MSSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "1C",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Windows",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "HTML",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Fortran",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Windows",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Eclipse",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Engineering_manager",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Engineering_manager",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Engineering_manager",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Engineering_manager",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Hadoop",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Spring",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Subversion",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Subversion",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Jira",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Git",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TypeScript",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Eclipse",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "IBM_DB2",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Confluence",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "IBM_DB2",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "iOS",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Marketing_or_sales_professional",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Linuxbased",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "MSSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "NodeJS",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Jira",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Educator_or_academic_researcher",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Hadoop",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Hadoop",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Apache_Hbase",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Apache_Hive",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Apache_Hbase",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Apache_Hive",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "System_administrator",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Marketing_or_sales_professional",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Marketing_or_sales_professional",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Google_HangoutsChat",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Marketing_or_sales_professional",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Swift",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Educator_or_academic_researcher",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Educator_or_academic_researcher",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Facebook",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Eclipse",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Eclipse",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "iOS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Educator_or_academic_researcher",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "SAP",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Hadoop",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Data_or_business_analyst",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Designer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Xamarin",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Xamarin",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Xamarin",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Cassandra",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Cassandra",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Cassandra",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MariaDB",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MariaDB",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MariaDB",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Confluence",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Confluence",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "NET_Core",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "iOS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "NET_Core",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Lua",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Atom",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "PyCharm",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PyCharm",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Visual_Studio_Code",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Linuxbased",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Windows",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Marketing_or_sales_professional",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Ruby",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "HipChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "HipChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HipChat",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Hadoop",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Marketing_or_sales_professional",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "NET_Core",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Redis",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Fullstack_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Data_or_business_analyst",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Educator_or_academic_researcher",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Spring",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Visual_Basic",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Database_Administrator",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Python",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Angular",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "DelphiObject_Pascal",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "HTML",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Pascal",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Python",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Visual_Basic",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "IBM_DB2",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "IBM_DB2",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "MySQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Cassandra",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "IntelliJ",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Subversion",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Subversion",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "RStudio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Trello",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "NET_Core",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Google_BigQuery",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Google_Cloud_Storage",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Notepad",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Xcode",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Sublime_Text",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Vim",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Google_BigQuery",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Google_Cloud_Storage",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Visual_Basic",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Database_Administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Eclipse",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PyCharm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Haskell",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Visual_Studio_Code",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "PyCharm",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PyCharm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Fortran",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PHPStorm",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "MacOs",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Windows",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Eclipse",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "System_administrator",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Marketing_or_sales_professional",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "MacOs",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "F",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Perl",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "MSSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Designer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "SAP",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Data_or_business_analyst",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "VBNET",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Designer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Database_Administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "React",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "SAP",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "1C",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Marketing_or_sales_professional",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "SAP",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Google_BigQuery",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Google_Cloud_Storage",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Google_HangoutsChat",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Notepad",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Sublime_Text",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "R",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Educator_or_academic_researcher",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "HTML",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "SQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Apache_Hbase",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Apache_Hive",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Educator_or_academic_researcher",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Apache_Hbase",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Apache_Hive",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Memcached",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Subversion",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Subversion",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "R",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Fortran",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Hadoop",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Subversion",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Eclipse",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MariaDB",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MariaDB",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Java",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "IBM_DB2",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "RStudio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "RStudio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "RStudio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "RStudio",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "VBNET",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Eclipse",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Notepad",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "BashShell",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Eclipse",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Xamarin",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Game_or_graphics_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Game_or_graphics_developer",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Eclipse",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Notepad",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Sublime_Text",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Matlab",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Python",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Windows",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Zend",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "CSS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Visual_Basic",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Designer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "VBNET",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Atom",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Subversion",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Memcached",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Database_Administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "PyCharm",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "CSS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Designer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Marketing_or_sales_professional",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "SQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Coda",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Marketing_or_sales_professional",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Google_HangoutsChat",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Frontend_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Mobile_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Mobile_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Memcached",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Android_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MariaDB",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Mobile_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Facebook",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "IntelliJ",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "IntelliJ",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Confluence",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Data_or_business_analyst",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Perl",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "R",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Zend",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "R",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "RStudio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "RStudio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Google_HangoutsChat",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Frontend_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "System_administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "VBNET",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Marketing_or_sales_professional",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Subversion",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "DevOps_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "DevOps_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "DevOps_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DevOps_specialist",
                            "target": "Confluence",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DevOps_specialist",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DevOps_specialist",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "BashShell",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "PHPStorm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "PHPStorm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Erlang",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Go",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Haskell",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Python",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "PostgreSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Haskell",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Java",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Scala",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "HTML",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Perl",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "R",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Ruby",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "SQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Python",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Subversion",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Google_BigQuery",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Google_Cloud_Storage",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Google_BigQuery",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Google_Cloud_Storage",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Google_BigQuery",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Google_Cloud_Storage",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "React",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Google_HangoutsChat",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Designer",
                            "weight": "1"
                        }
                    }
                ]
            }
        }
    }
}
`);

export default data;
