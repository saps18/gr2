import bmk from "./bmk";

const n1_1: { bmkNodes: GraphNode[]; bmkEdges: GraphEdge[] } = {
    bmkNodes: [
        {
            id: 'Универсальные компетенции',
            label: 'Универсальные компетенции',
            hidden: false,
            group: 'BMK1',
            physics: false,
            shape: 'hexagon',
            size: 20,
            // fixed: true,
            x: 0,
            y: 70,
        },
        {
            id: 'Работа с новизной',
            label: 'Работа с новизной',
            hidden: false,
            group: 'BMK',
            physics: true,
            shape: 'dot',
            size: 25,
        },
        {
            id: 'Самоактуализация',
            label: 'Самоактуализация',
            hidden: false,
            group: 'BMK',
            physics: true,
            shape: 'dot',
            size: 25,
        }],
    bmkEdges: [
        {
            id: 'Работа с новизной1',
            from: 'Универсальные компетенции',
            to: 'Работа с новизной',
            hidden: false,
            value: 1,
            type: 'bmkEdge',
        },
        {
            id: 'Самоактуализация1',
            from: 'Универсальные компетенции',
            to: 'Самоактуализация',
            hidden: false,
            value: 2,
            type: 'bmkEdge',
        }]
};

export default n1_1;