const data: ParsedGexf = JSON.parse(`{
    "_declaration": {
        "_attributes": {
            "version": "1.0",
            "encoding": "UTF-8",
            "standalone": "no"
        }
    },
    "graphview": {
        "graph": {
            "nodes": {
                "node": [
                    {
                        "_attributes": {
                            "id": "BashShell",
                            "label": "Bash/Shell",
                            "weight": "1103"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1103"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "C",
                            "label": "C#",
                            "weight": "3723"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3723"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "CSS",
                            "label": "CSS",
                            "weight": "1205"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1205"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "DelphiObject_Pascal",
                            "label": "Delphi/Object Pascal",
                            "weight": "868"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "868"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Go",
                            "label": "Go",
                            "weight": "332"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "332"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "HTML",
                            "label": "HTML",
                            "weight": "1740"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1740"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Pascal",
                            "label": "Pascal",
                            "weight": "468"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "468"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Python",
                            "label": "Python",
                            "weight": "8935"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "8935"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Eclipse",
                            "label": "Eclipse",
                            "weight": "70"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "70"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IntelliJ",
                            "label": "IntelliJ",
                            "weight": "128"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "128"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NetBeans",
                            "label": "NetBeans",
                            "weight": "20"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "20"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PHPStorm",
                            "label": "PHPStorm",
                            "weight": "61"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "61"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Zend",
                            "label": "Zend",
                            "weight": "30"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "30"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Memcached",
                            "label": "Memcached",
                            "weight": "20"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "20"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MSSQL",
                            "label": "MSSQL",
                            "weight": "151"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "151"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MySQL",
                            "label": "MySQL",
                            "weight": "1381"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1381"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PostgreSQL",
                            "label": "PostgreSQL",
                            "weight": "912"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "912"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Redis",
                            "label": "Redis",
                            "weight": "168"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "168"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Angular",
                            "label": "Angular",
                            "weight": "114"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "114"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Confluence",
                            "label": "Confluence",
                            "weight": "191"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "191"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Jira",
                            "label": "Jira",
                            "weight": "365"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "365"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Trello",
                            "label": "Trello",
                            "weight": "82"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "82"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Git",
                            "label": "Git",
                            "weight": "967"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "967"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Mercurial",
                            "label": "Mercurial",
                            "weight": "102"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "102"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Linuxbased",
                            "label": "Linux-based",
                            "weight": "3647"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3647"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "SQL",
                            "label": "SQL",
                            "weight": "3294"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "3294"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "System_administrator",
                            "label": "System administrator",
                            "weight": "396"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "396"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "1C",
                            "label": "1C",
                            "weight": "4040"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4040"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Windows",
                            "label": "Windows",
                            "weight": "2162"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2162"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Java",
                            "label": "Java",
                            "weight": "2461"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2461"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "label": "Office / productivity suite (Microsoft Office, Google Suite, etc)",
                            "weight": "355"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "355"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Studio",
                            "label": "Visual Studio",
                            "weight": "337"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "337"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "BSDUnix",
                            "label": "BSD/Unix",
                            "weight": "561"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "561"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Android",
                            "label": "Android",
                            "weight": "644"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "644"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IBM_DB2",
                            "label": "IBM DB2",
                            "weight": "86"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "86"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Database_Administrator",
                            "label": "Database Administrator",
                            "weight": "321"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "321"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Matlab",
                            "label": "Matlab",
                            "weight": "423"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "423"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "R",
                            "label": "R",
                            "weight": "649"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "649"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Basic",
                            "label": "Visual Basic",
                            "weight": "86"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "86"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "SQLite",
                            "label": "SQLite",
                            "weight": "203"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "203"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "QA_or_test_developer",
                            "label": "QA or test developer",
                            "weight": "27698"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "27698"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Team_Foundation_Version_Control",
                            "label": "Team Foundation Version Control",
                            "weight": "246"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "246"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Marketing_or_sales_professional",
                            "label": "Marketing or sales professional",
                            "weight": "402"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "402"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Slack",
                            "label": "Slack",
                            "weight": "16"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "16"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Educator_or_academic_researcher",
                            "label": "Educator or academic researcher",
                            "weight": "374"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "374"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Perl",
                            "label": "Perl",
                            "weight": "573"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "573"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Django",
                            "label": "Django",
                            "weight": "868"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "868"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "label": "Microsoft Azure (Tables, CosmosDB, SQL, etc)",
                            "weight": "75"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "75"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Ruby",
                            "label": "Ruby",
                            "weight": "602"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "602"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Android_Studio",
                            "label": "Android Studio",
                            "weight": "79"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "79"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NodeJS",
                            "label": "NodeJS",
                            "weight": "100"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "100"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Csuite_executive_CEO_CTO_etc",
                            "label": "C-suite executive (CEO, CTO, etc)",
                            "weight": "411"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "411"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_BigQuery",
                            "label": "Google BigQuery",
                            "weight": "78"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "78"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_Cloud_Storage",
                            "label": "Google Cloud Storage",
                            "weight": "67"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "67"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Google_HangoutsChat",
                            "label": "Google Hangouts/Chat",
                            "weight": "58"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "58"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "label": "Other wiki tool (Github, Google Sites, proprietary software, etc)",
                            "weight": "185"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "185"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TensorFlow",
                            "label": "TensorFlow",
                            "weight": "88"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "88"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Kotlin",
                            "label": "Kotlin",
                            "weight": "66"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "66"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Scala",
                            "label": "Scala",
                            "weight": "253"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "253"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ElasticSearch",
                            "label": "ElasticSearch",
                            "weight": "134"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "134"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "iOS",
                            "label": "iOS",
                            "weight": "451"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "451"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Spring",
                            "label": "Spring",
                            "weight": "300"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "300"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Designer",
                            "label": "Designer",
                            "weight": "108"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "108"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "SAP",
                            "label": "SAP",
                            "weight": "512"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "512"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Facebook",
                            "label": "Facebook",
                            "weight": "144"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "144"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ObjectiveC",
                            "label": "Objective-C",
                            "weight": "170"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "170"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "React",
                            "label": "React",
                            "weight": "247"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "247"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TorchPyTorch",
                            "label": "Torch/PyTorch",
                            "weight": "50"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "50"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Xamarin",
                            "label": "Xamarin",
                            "weight": "15"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "15"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Data_scientist_or_machine_learning_specialist",
                            "label": "Data scientist or machine learning specialist",
                            "weight": "796"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "796"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Light_Table",
                            "label": "Light Table",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Data_or_business_analyst",
                            "label": "Data or business analyst",
                            "weight": "1578"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1578"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MariaDB",
                            "label": "MariaDB",
                            "weight": "29"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "29"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "PyCharm",
                            "label": "PyCharm",
                            "weight": "98"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "98"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "MacOs",
                            "label": "MacOs",
                            "weight": "149"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Operation systems"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "149"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Subversion",
                            "label": "Subversion",
                            "weight": "11"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Version control"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "11"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "255",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Rust",
                            "label": "Rust",
                            "weight": "10"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "10"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Lua",
                            "label": "Lua",
                            "weight": "116"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "116"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Hadoop",
                            "label": "Hadoop",
                            "weight": "464"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "464"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Sublime_Text",
                            "label": "Sublime Text",
                            "weight": "4"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Vim",
                            "label": "Vim",
                            "weight": "11"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "11"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Swift",
                            "label": "Swift",
                            "weight": "209"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "209"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Atom",
                            "label": "Atom",
                            "weight": "8"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "8"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "NET_Core",
                            "label": ".NET Core",
                            "weight": "23"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "23"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Visual_Studio_Code",
                            "label": "Visual Studio Code",
                            "weight": "21"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "21"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cordova",
                            "label": "Cordova",
                            "weight": "8"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "8"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "VBNET",
                            "label": "VB.NET",
                            "weight": "17"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "17"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Fortran",
                            "label": "Fortran",
                            "weight": "96"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "96"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Other_chat_system_IRC_proprietary_software_etc",
                            "label": "Other chat system (IRC, proprietary software, etc)",
                            "weight": "12"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "12"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Amazon_DynamoDB",
                            "label": "Amazon DynamoDB",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "TypeScript",
                            "label": "TypeScript",
                            "weight": "77"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "77"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Apache_Hbase",
                            "label": "Apache Hbase",
                            "weight": "21"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "21"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Apache_Hive",
                            "label": "Apache Hive",
                            "weight": "40"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "40"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Mobile_developer",
                            "label": "Mobile developer",
                            "weight": "350"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "350"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cassandra",
                            "label": "Cassandra",
                            "weight": "28"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "28"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Cobol",
                            "label": "Cobol",
                            "weight": "4"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Xcode",
                            "label": "Xcode",
                            "weight": "22"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "22"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Spark",
                            "label": "Spark",
                            "weight": "179"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Frameworks"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "179"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "128",
                                "g": "128",
                                "b": "128"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "RStudio",
                            "label": "RStudio",
                            "weight": "16"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "16"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Haskell",
                            "label": "Haskell",
                            "weight": "13"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "13"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "ABAP",
                            "label": "ABAP",
                            "weight": "5"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "5"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "CoffeeScript",
                            "label": "CoffeeScript",
                            "weight": "7"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "7"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Erlang",
                            "label": "Erlang",
                            "weight": "17"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "17"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Assembly",
                            "label": "Assembly",
                            "weight": "31"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "31"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "IPythonJupiter",
                            "label": "IPython/Jupiter",
                            "weight": "5"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "5"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Groovy",
                            "label": "Groovy",
                            "weight": "14"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "14"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Clojure",
                            "label": "Clojure",
                            "weight": "2"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Notepad",
                            "label": "Notepad++",
                            "weight": "2"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "IDE"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "2"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Backend_developer",
                            "label": "Back-end developer",
                            "weight": "155"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "155"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Frontend_developer",
                            "label": "Front-end developer",
                            "weight": "36"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "36"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "F",
                            "label": "F#",
                            "weight": "1"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Language"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "1"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "205",
                                "g": "92",
                                "b": "92"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Stack_Overflow_Enterprise",
                            "label": "Stack Overflow Enterprise",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Communication tools"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "255",
                                "b": "0"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Engineering_manager",
                            "label": "Engineering manager",
                            "weight": "9"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "9"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Amazon_RDSAurora",
                            "label": "Amazon RDS/Aurora",
                            "weight": "29"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "29"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Desktop_or_enterprise_applications_developer",
                            "label": "Desktop or enterprise applications developer",
                            "weight": "60"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "60"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Embedded_application_or_devices_developer",
                            "label": "Embedded application or devices developer",
                            "weight": "18"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "18"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "DevOps_specialist",
                            "label": "DevOps specialist",
                            "weight": "74"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "74"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Game_or_graphics_developer",
                            "label": "Game or graphics developer",
                            "weight": "4"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "4"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Amazon_Redshift",
                            "label": "Amazon Redshift",
                            "weight": "8"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "8"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Fullstack_developer",
                            "label": "Full-stack developer",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Role position"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "250",
                                "g": "128",
                                "b": "114"
                            }
                        }
                    },
                    {
                        "_attributes": {
                            "id": "Neo4j",
                            "label": "Neo4j",
                            "weight": "6"
                        },
                        "attvalues": {
                            "attvalue": [
                                {
                                    "_attributes": {
                                        "for": "node_type",
                                        "value": "Database"
                                    }
                                },
                                {
                                    "_attributes": {
                                        "for": "weight",
                                        "value": "6"
                                    }
                                }
                            ]
                        },
                        "viz:color": {
                            "_attributes": {
                                "r": "0",
                                "g": "0",
                                "b": "255"
                            }
                        }
                    }
                ]
            },
            "edges": {
                "edge": [
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "C",
                            "weight": "329"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "CSS",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "DelphiObject_Pascal",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Go",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "HTML",
                            "weight": "159"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Pascal",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Python",
                            "weight": "1028"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Eclipse",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "IntelliJ",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NetBeans",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MSSQL",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MySQL",
                            "weight": "225"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PostgreSQL",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Redis",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Angular",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Confluence",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Jira",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Git",
                            "weight": "149"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Mercurial",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Linuxbased",
                            "weight": "682"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "CSS",
                            "weight": "482"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "DelphiObject_Pascal",
                            "weight": "546"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Go",
                            "weight": "167"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "HTML",
                            "weight": "675"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Pascal",
                            "weight": "296"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Python",
                            "weight": "2607"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Eclipse",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "IntelliJ",
                            "weight": "120"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NetBeans",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PHPStorm",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Zend",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MSSQL",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MySQL",
                            "weight": "576"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PostgreSQL",
                            "weight": "287"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Redis",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Angular",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Confluence",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Jira",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Trello",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Git",
                            "weight": "340"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Mercurial",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Linuxbased",
                            "weight": "861"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "DelphiObject_Pascal",
                            "weight": "102"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Go",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "HTML",
                            "weight": "1100"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Pascal",
                            "weight": "145"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Python",
                            "weight": "1019"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Eclipse",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "IntelliJ",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "NetBeans",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Zend",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Memcached",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MSSQL",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MySQL",
                            "weight": "325"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PostgreSQL",
                            "weight": "220"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Redis",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Angular",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Confluence",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Jira",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Trello",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Git",
                            "weight": "238"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Mercurial",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Linuxbased",
                            "weight": "278"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Go",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "HTML",
                            "weight": "160"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Pascal",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Python",
                            "weight": "577"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Eclipse",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "IntelliJ",
                            "weight": "105"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "NetBeans",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PHPStorm",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Zend",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MSSQL",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MySQL",
                            "weight": "310"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PostgreSQL",
                            "weight": "143"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Redis",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Angular",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Jira",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Git",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Linuxbased",
                            "weight": "357"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "HTML",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Pascal",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Python",
                            "weight": "225"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Eclipse",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "IntelliJ",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NetBeans",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "MySQL",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "PostgreSQL",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Redis",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Angular",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Confluence",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Jira",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Git",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Mercurial",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Linuxbased",
                            "weight": "101"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Pascal",
                            "weight": "159"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Python",
                            "weight": "1361"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Eclipse",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "IntelliJ",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "NetBeans",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Zend",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Memcached",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MSSQL",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MySQL",
                            "weight": "361"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PostgreSQL",
                            "weight": "282"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Redis",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Angular",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Confluence",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Jira",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Trello",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Git",
                            "weight": "243"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Mercurial",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Linuxbased",
                            "weight": "396"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Python",
                            "weight": "299"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Eclipse",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "IntelliJ",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NetBeans",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MySQL",
                            "weight": "129"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PostgreSQL",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Redis",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Confluence",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Jira",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Git",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Mercurial",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Linuxbased",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Eclipse",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "IntelliJ",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NetBeans",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PHPStorm",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Zend",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Memcached",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MSSQL",
                            "weight": "103"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MySQL",
                            "weight": "981"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PostgreSQL",
                            "weight": "653"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Redis",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Angular",
                            "weight": "84"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Confluence",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Jira",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Trello",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Git",
                            "weight": "663"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Mercurial",
                            "weight": "93"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Linuxbased",
                            "weight": "1992"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "IntelliJ",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "NetBeans",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "MySQL",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PostgreSQL",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Redis",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Angular",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Git",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Linuxbased",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "NetBeans",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PHPStorm",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "MySQL",
                            "weight": "107"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PostgreSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Redis",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Git",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Linuxbased",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PHPStorm",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "MySQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PostgreSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Redis",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Git",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Mercurial",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Linuxbased",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Zend",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "MySQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "PostgreSQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Redis",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Jira",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Git",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Mercurial",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Memcached",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MSSQL",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MySQL",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "PostgreSQL",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Redis",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Angular",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Git",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Linuxbased",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "MSSQL",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "MySQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "PostgreSQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Redis",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Angular",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Jira",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Git",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Memcached",
                            "target": "Linuxbased",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "MySQL",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "PostgreSQL",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Redis",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Angular",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Confluence",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Jira",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Git",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Mercurial",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Linuxbased",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "PostgreSQL",
                            "weight": "309"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Redis",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Angular",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Confluence",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Jira",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Trello",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Git",
                            "weight": "299"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Mercurial",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Linuxbased",
                            "weight": "524"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Redis",
                            "weight": "78"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Angular",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Confluence",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Jira",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Trello",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Git",
                            "weight": "123"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Mercurial",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Linuxbased",
                            "weight": "290"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Angular",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Confluence",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Jira",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Trello",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Git",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Mercurial",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Linuxbased",
                            "weight": "67"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Confluence",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Jira",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Trello",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Git",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Mercurial",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Linuxbased",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Jira",
                            "weight": "82"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Trello",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Git",
                            "weight": "69"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Linuxbased",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Trello",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Git",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Mercurial",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Linuxbased",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Git",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Mercurial",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Linuxbased",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Mercurial",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Linuxbased",
                            "weight": "428"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Linuxbased",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SQL",
                            "weight": "1127"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "System_administrator",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "1C",
                            "weight": "430"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Windows",
                            "weight": "719"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SQL",
                            "weight": "2287"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "System_administrator",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "1C",
                            "weight": "534"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Windows",
                            "weight": "1068"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MSSQL",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "System_administrator",
                            "weight": "86"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "1C",
                            "weight": "493"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Linuxbased",
                            "weight": "1138"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Windows",
                            "weight": "802"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "System_administrator",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "1C",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Windows",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "1C",
                            "weight": "129"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Linuxbased",
                            "weight": "140"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Windows",
                            "weight": "168"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Linuxbased",
                            "weight": "279"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Windows",
                            "weight": "498"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Linuxbased",
                            "target": "Windows",
                            "weight": "1258"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Java",
                            "weight": "1349"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "1C",
                            "weight": "213"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SQL",
                            "weight": "541"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SQL",
                            "weight": "681"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Studio",
                            "weight": "101"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "1C",
                            "weight": "282"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "BSDUnix",
                            "weight": "118"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Windows",
                            "weight": "406"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Studio",
                            "weight": "209"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "BSDUnix",
                            "weight": "276"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "1C",
                            "weight": "137"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "BSDUnix",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Linuxbased",
                            "weight": "127"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Windows",
                            "weight": "115"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "BSDUnix",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "Linuxbased",
                            "weight": "217"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "Windows",
                            "weight": "285"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SQL",
                            "weight": "566"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "IBM_DB2",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Database_Administrator",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "IBM_DB2",
                            "weight": "70"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MySQL",
                            "weight": "531"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Database_Administrator",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "MySQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Database_Administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Database_Administrator",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "R",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Visual_Basic",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Visual_Basic",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Matlab",
                            "weight": "192"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "R",
                            "weight": "132"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SQLite",
                            "weight": "157"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Java",
                            "weight": "344"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Matlab",
                            "weight": "78"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "R",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SQLite",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Windows",
                            "weight": "382"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Matlab",
                            "weight": "96"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Python",
                            "weight": "1852"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "R",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SQL",
                            "weight": "772"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MySQL",
                            "weight": "394"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SQLite",
                            "weight": "103"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Linuxbased",
                            "weight": "603"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Windows",
                            "weight": "288"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Python",
                            "weight": "237"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SQL",
                            "weight": "104"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "MySQL",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SQLite",
                            "weight": "67"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Linuxbased",
                            "weight": "131"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Windows",
                            "weight": "97"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "R",
                            "weight": "240"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SQLite",
                            "weight": "196"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SQL",
                            "weight": "146"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MySQL",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SQLite",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Linuxbased",
                            "weight": "94"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Windows",
                            "weight": "102"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "SQLite",
                            "weight": "104"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "SQLite",
                            "weight": "160"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Windows",
                            "weight": "449"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Linuxbased",
                            "weight": "97"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Windows",
                            "weight": "120"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "QA_or_test_developer",
                            "weight": "341"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "QA_or_test_developer",
                            "weight": "260"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "QA_or_test_developer",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "130"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Team_Foundation_Version_Control",
                            "target": "Linuxbased",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Team_Foundation_Version_Control",
                            "target": "Windows",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "1C",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Confluence",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Jira",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Slack",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Trello",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Confluence",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Jira",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Slack",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Trello",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Slack",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Slack",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Trello",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "QA_or_test_developer",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Linuxbased",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Linuxbased",
                            "weight": "198"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Educator_or_academic_researcher",
                            "weight": "69"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "System_administrator",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "1C",
                            "weight": "344"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Perl",
                            "weight": "323"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SQL",
                            "weight": "395"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Windows",
                            "weight": "203"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Python",
                            "weight": "498"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SQL",
                            "weight": "330"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MySQL",
                            "weight": "88"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "PostgreSQL",
                            "weight": "97"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Linuxbased",
                            "weight": "371"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Windows",
                            "weight": "93"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PostgreSQL",
                            "weight": "392"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Windows",
                            "weight": "263"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "System_administrator",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "System_administrator",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Django",
                            "weight": "154"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "BSDUnix",
                            "weight": "151"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SQLite",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "1C",
                            "weight": "246"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Django",
                            "weight": "237"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "BSDUnix",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Windows",
                            "weight": "273"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SQLite",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Django",
                            "weight": "245"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SQLite",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "1C",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Django",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "BSDUnix",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Windows",
                            "weight": "143"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Django",
                            "weight": "782"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Django",
                            "weight": "226"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "BSDUnix",
                            "weight": "133"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "SQLite",
                            "weight": "94"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "1C",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Django",
                            "weight": "194"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "BSDUnix",
                            "weight": "99"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "1C",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Django",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "BSDUnix",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Django",
                            "weight": "153"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "BSDUnix",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Windows",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Studio",
                            "weight": "251"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "90"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Studio",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "72"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "104"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "107"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Studio",
                            "weight": "208"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Django",
                            "weight": "219"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Django",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Windows",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Java",
                            "weight": "379"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Git",
                            "weight": "281"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Git",
                            "weight": "308"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Java",
                            "weight": "248"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Perl",
                            "weight": "156"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Perl",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "MySQL",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "PostgreSQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Linuxbased",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Windows",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Linuxbased",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Ruby",
                            "weight": "204"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Android_Studio",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NodeJS",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Android",
                            "weight": "269"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Ruby",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Android_Studio",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "NodeJS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Android",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Ruby",
                            "weight": "103"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Android_Studio",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "NodeJS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Android",
                            "weight": "164"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Ruby",
                            "weight": "307"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Android_Studio",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Eclipse",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "PostgreSQL",
                            "weight": "146"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Angular",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Django",
                            "weight": "76"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NodeJS",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Android",
                            "weight": "313"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Ruby",
                            "weight": "473"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Android_Studio",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NodeJS",
                            "weight": "76"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Android",
                            "weight": "347"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Android_Studio",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Eclipse",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MySQL",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "PostgreSQL",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SQLite",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Angular",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Django",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "NodeJS",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Git",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Android",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Linuxbased",
                            "weight": "187"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Eclipse",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "PostgreSQL",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "SQLite",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Angular",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Django",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "NodeJS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Android",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Linuxbased",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "SQLite",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "1C",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "NodeJS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Android",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "NodeJS",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Android",
                            "weight": "37"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "NodeJS",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Android",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Angular",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "NodeJS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Git",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Android",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Angular",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "NodeJS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Git",
                            "weight": "102"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Android",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Django",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "NodeJS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Android",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "NodeJS",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Git",
                            "weight": "181"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Android",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Linuxbased",
                            "weight": "92"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Git",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Android",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Linuxbased",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Android",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "Linuxbased",
                            "weight": "126"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "411"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Visual_Studio",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "1C",
                            "weight": "214"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Studio",
                            "weight": "90"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "118"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "299"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "1C",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_BigQuery",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_Cloud_Storage",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_BigQuery",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_Cloud_Storage",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "98"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_BigQuery",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_Cloud_Storage",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Google_Cloud_Storage",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Google_HangoutsChat",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Google_HangoutsChat",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_HangoutsChat",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Linuxbased",
                            "weight": "67"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TensorFlow",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "TensorFlow",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Git",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Git",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Linuxbased",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Windows",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Windows",
                            "weight": "107"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Java",
                            "weight": "246"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Kotlin",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Scala",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "ElasticSearch",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "BSDUnix",
                            "weight": "77"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Kotlin",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Scala",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "ElasticSearch",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Kotlin",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Scala",
                            "weight": "203"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "ElasticSearch",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Redis",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Jira",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Mercurial",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "BSDUnix",
                            "weight": "67"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Python",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Scala",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "SQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "ElasticSearch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "PostgreSQL",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Redis",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Git",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Scala",
                            "weight": "159"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "ElasticSearch",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "SQL",
                            "weight": "160"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "ElasticSearch",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "PostgreSQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Redis",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Git",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Mercurial",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "BSDUnix",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Linuxbased",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "ElasticSearch",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Redis",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Jira",
                            "weight": "98"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Mercurial",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "BSDUnix",
                            "weight": "120"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "PostgreSQL",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Redis",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Git",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "BSDUnix",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "BSDUnix",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "BSDUnix",
                            "weight": "54"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "BSDUnix",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Android",
                            "weight": "115"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "iOS",
                            "weight": "80"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "iOS",
                            "weight": "139"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "QA_or_test_developer",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "QA_or_test_developer",
                            "weight": "53"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "QA_or_test_developer",
                            "weight": "214"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "QA_or_test_developer",
                            "weight": "540"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Spring",
                            "weight": "284"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Spring",
                            "weight": "240"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Spring",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Git",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Git",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Designer",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Designer",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Git",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Designer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Git",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Basic",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Basic",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Basic",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "MySQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "SQLite",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "1C",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "IntelliJ",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "1C",
                            "weight": "91"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Facebook",
                            "weight": "83"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Linuxbased",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Windows",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "ObjectiveC",
                            "weight": "170"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "React",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TorchPyTorch",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Xamarin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Matlab",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "ObjectiveC",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "React",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TensorFlow",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "iOS",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Matlab",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "ObjectiveC",
                            "weight": "90"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "React",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "TensorFlow",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "iOS",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "ObjectiveC",
                            "weight": "162"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "React",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TensorFlow",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TorchPyTorch",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Xamarin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "iOS",
                            "weight": "66"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "ObjectiveC",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "PostgreSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "TensorFlow",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "BSDUnix",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Python",
                            "weight": "164"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SQLite",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Xamarin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Android",
                            "weight": "162"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "BSDUnix",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "iOS",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "React",
                            "weight": "114"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "TensorFlow",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "TorchPyTorch",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Xamarin",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "React",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TensorFlow",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Xamarin",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "iOS",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "React",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "TensorFlow",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "TorchPyTorch",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "iOS",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "React",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "TensorFlow",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "TorchPyTorch",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "React",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "iOS",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "React",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Android",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "iOS",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "TensorFlow",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "TorchPyTorch",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Xamarin",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "iOS",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "TorchPyTorch",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Android",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "iOS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "iOS",
                            "weight": "224"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "iOS",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "1C",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Linuxbased",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MariaDB",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "MSSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "MySQL",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "PostgreSQL",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Jira",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "BSDUnix",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "BSDUnix",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "BSDUnix",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Windows",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Windows",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "SAP",
                            "weight": "85"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "SAP",
                            "weight": "88"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "SAP",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "IntelliJ",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "1C",
                            "weight": "89"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "SAP",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "SAP",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "SAP",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "SAP",
                            "weight": "168"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Database_Administrator",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Designer",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Database_Administrator",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Database_Administrator",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Designer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Linuxbased",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Linuxbased",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "PyCharm",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "PyCharm",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "PyCharm",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Django",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "PyCharm",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SQL",
                            "weight": "129"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "PyCharm",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "PyCharm",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PyCharm",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "1C",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Django",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Windows",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Windows",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MacOs",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Perl",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "SAP",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MacOs",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "SAP",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MacOs",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Perl",
                            "weight": "85"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "QA_or_test_developer",
                            "weight": "308"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MacOs",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Perl",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Visual_Studio",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "QA_or_test_developer",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "1C",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "SAP",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Visual_Studio",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "QA_or_test_developer",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "1C",
                            "weight": "76"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SAP",
                            "weight": "59"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "SAP",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MacOs",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "QA_or_test_developer",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "SAP",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "1C",
                            "weight": "1038"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "SAP",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "MacOs",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "MacOs",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Linuxbased",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Linuxbased",
                            "target": "MacOs",
                            "weight": "121"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "MacOs",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MacOs",
                            "target": "Windows",
                            "weight": "116"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Subversion",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Subversion",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Pascal",
                            "weight": "93"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "SAP",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Subversion",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "SAP",
                            "weight": "86"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Subversion",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "Subversion",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Subversion",
                            "target": "Linuxbased",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Rust",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Rust",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Rust",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Lua",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Hadoop",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Lua",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Hadoop",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Lua",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Hadoop",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Android",
                            "weight": "69"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Lua",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MSSQL",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Hadoop",
                            "weight": "142"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "MSSQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "MySQL",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "PostgreSQL",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Hadoop",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Git",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Android",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Hadoop",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Hadoop",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Hadoop",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Git",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Android",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Windows",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "Windows",
                            "weight": "108"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Git",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "IBM_DB2",
                            "weight": "60"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Spring",
                            "weight": "56"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Spring",
                            "weight": "49"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "IBM_DB2",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Spring",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "IBM_DB2",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "IBM_DB2",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Spring",
                            "weight": "175"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "PostgreSQL",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Spring",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Android",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Linuxbased",
                            "weight": "63"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Windows",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Spring",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Android",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Linuxbased",
                            "weight": "208"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Windows",
                            "weight": "55"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "PostgreSQL",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "SQLite",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Mercurial",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Mercurial",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mercurial",
                            "target": "Windows",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Vim",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Vim",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Vim",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Vim",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Sublime_Text",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Vim",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Vim",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "System_administrator",
                            "weight": "46"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "System_administrator",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "System_administrator",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Sublime_Text",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Git",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "QA_or_test_developer",
                            "weight": "132"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "BSDUnix",
                            "weight": "57"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "iOS",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "BSDUnix",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "iOS",
                            "weight": "152"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "Windows",
                            "weight": "87"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Jira",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "MacOs",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BSDUnix",
                            "target": "MacOs",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "BSDUnix",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Mercurial",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Linuxbased",
                            "weight": "35"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Swift",
                            "weight": "179"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Swift",
                            "weight": "182"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Swift",
                            "weight": "173"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Trello",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Swift",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Android",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "MySQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Trello",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Git",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Android",
                            "weight": "163"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Slack",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Database_Administrator",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Atom",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Atom",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "33"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Atom",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Atom",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Atom",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Git",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Kotlin",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Lua",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NodeJS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Java",
                            "weight": "74"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Lua",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Scala",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "SQL",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Python",
                            "weight": "86"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Scala",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "SQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Mercurial",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Linuxbased",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "MySQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NodeJS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Mercurial",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Swift",
                            "weight": "95"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Swift",
                            "weight": "165"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Git",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Swift",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "iOS",
                            "weight": "61"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Git",
                            "target": "iOS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Spring",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "1C",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "1C",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Google_HangoutsChat",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Marketing_or_sales_professional",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Marketing_or_sales_professional",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Marketing_or_sales_professional",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Basic",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Visual_Basic",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Jira",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "NET_Core",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "42"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Perl",
                            "weight": "78"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Perl",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "NET_Core",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Angular",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Git",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "NET_Core",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NET_Core",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Angular",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "NET_Core",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Angular",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Windows",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Visual_Studio_Code",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Visual_Studio_Code",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Visual_Studio",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Visual_Studio_Code",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Django",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Visual_Studio_Code",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Django",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Angular",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "React",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Cordova",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "React",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "React",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Git",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Linuxbased",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Git",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "1C",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Pascal",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Perl",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "IBM_DB2",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "SQLite",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "QA_or_test_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Perl",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "IBM_DB2",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "QA_or_test_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "IBM_DB2",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "SQLite",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "SQLite",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "QA_or_test_developer",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "QA_or_test_developer",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "VBNET",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "VBNET",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Visual_Studio",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "1C",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Windows",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "VBNET",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "1C",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MacOs",
                            "weight": "36"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Jira",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Jira",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "SAP",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Jira",
                            "weight": "47"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Designer",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Designer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Designer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "iOS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Windows",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "Linuxbased",
                            "weight": "79"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "SQLite",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Spring",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Spring",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Spring",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Git",
                            "weight": "112"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Mercurial",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Matlab",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "QA_or_test_developer",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "1C",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Vim",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Vim",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "MSSQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "1C",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Vim",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MSSQL",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "System_administrator",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "iOS",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "MacOs",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "iOS",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "MacOs",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "MacOs",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "iOS",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "iOS",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "iOS",
                            "target": "MacOs",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "MSSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MSSQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "MySQL",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Windows",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "Linuxbased",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_chat_system_IRC_proprietary_software_etc",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Slack",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Hadoop",
                            "weight": "136"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Hadoop",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Hadoop",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Spring",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Jira",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Facebook",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Windows",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "NodeJS",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "React",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "React",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Trello",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Data_or_business_analyst",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Data_or_business_analyst",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Data_or_business_analyst",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Data_or_business_analyst",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Data_or_business_analyst",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "R",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Android",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "iOS",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Android",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Android",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "System_administrator",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "BSDUnix",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Apache_Hbase",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Apache_Hive",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Apache_Hbase",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Apache_Hive",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Apache_Hive",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "MySQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "PostgreSQL",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Designer",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "1C",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Android",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Eclipse",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Visual_Studio",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Linuxbased",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Eclipse",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Visual_Studio",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Linuxbased",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Visual_Studio",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "iOS",
                            "weight": "26"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Android",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "iOS",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "1C",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "1C",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Linuxbased",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Cassandra",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "MariaDB",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Cassandra",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Cassandra",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Zend",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Cassandra",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "MariaDB",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Cassandra",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "MariaDB",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Cassandra",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "SQLite",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Django",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MariaDB",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MSSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "MySQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "PostgreSQL",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "SQLite",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "SQLite",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Angular",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Django",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "React",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "React",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Django",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Atom",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Visual_Studio_Code",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Studio_Code",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "NetBeans",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "PyCharm",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Visual_Studio_Code",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Eclipse",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "NetBeans",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Visual_Studio_Code",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Visual_Studio",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Visual_Studio_Code",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Linuxbased",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Windows",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Linuxbased",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Ruby",
                            "weight": "106"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "BSDUnix",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Windows",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Linuxbased",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Windows",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Hadoop",
                            "weight": "160"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Hadoop",
                            "weight": "45"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Spring",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SQL",
                            "weight": "162"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Redis",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Jira",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NodeJS",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Windows",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Memcached",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Confluence",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Confluence",
                            "weight": "65"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Python",
                            "weight": "34"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "R",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "R",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "1C",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Windows",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Cobol",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Fortran",
                            "weight": "29"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "DelphiObject_Pascal",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Fortran",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "HTML",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Java",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Pascal",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Python",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "SQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Visual_Basic",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "IBM_DB2",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "MySQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "PostgreSQL",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "BSDUnix",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cobol",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Fortran",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Visual_Basic",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "HTML",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Java",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Pascal",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "SQL",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Visual_Basic",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "IBM_DB2",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "MySQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "PostgreSQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "BSDUnix",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Linuxbased",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Windows",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Basic",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "IBM_DB2",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "PostgreSQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "BSDUnix",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "BSDUnix",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Facebook",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Facebook",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Facebook",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Xcode",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "MacOs",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Django",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Mercurial",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "PyCharm",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Visual_Studio",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "TensorFlow",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "TensorFlow",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "MySQL",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "TensorFlow",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Mobile_developer",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Visual_Basic",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Visual_Studio_Code",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Kotlin",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "IntelliJ",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "PyCharm",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "PyCharm",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Visual_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Visual_Studio_Code",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Fortran",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Django",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Educator_or_academic_researcher",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Educator_or_academic_researcher",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "MacOs",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Swift",
                            "weight": "86"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Confluence",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Designer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "React",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Windows",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "QA_or_test_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "System_administrator",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "1C",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Android",
                            "weight": "133"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android",
                            "target": "MacOs",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Matlab",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SQLite",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Pascal",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Zend",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Zend",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Django",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Android",
                            "weight": "87"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "R",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "SAP",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Visual_Studio_Code",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "1C",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Ruby",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "QA_or_test_developer",
                            "weight": "50"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "System_administrator",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Spark",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Scala",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Django",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Scala",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Django",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Database_Administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Database_Administrator",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Database_Administrator",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Windows",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "IBM_DB2",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "SAP",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "BSDUnix",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Spark",
                            "weight": "125"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Google_HangoutsChat",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "RStudio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "RStudio",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "RStudio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "RStudio",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Jira",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Git",
                            "weight": "51"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "NET_Core",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "NET_Core",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NET_Core",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Educator_or_academic_researcher",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Matlab",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Python",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Linuxbased",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Windows",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "CSS",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Java",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "Designer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ABAP",
                            "target": "SAP",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "SAP",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "BSDUnix",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Android_Studio",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "IntelliJ",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "IntelliJ",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "PyCharm",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Django",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "Android",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Android",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Designer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "QA_or_test_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Ruby",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Linuxbased",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Visual_Studio",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Haskell",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "SQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "NetBeans",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Visual_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "NetBeans",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Xcode",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Xcode",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Linuxbased",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "QA_or_test_developer",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "MacOs",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Marketing_or_sales_professional",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Marketing_or_sales_professional",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Marketing_or_sales_professional",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "ElasticSearch",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Hadoop",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Hadoop",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "R",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Eclipse",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "RStudio",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Eclipse",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "RStudio",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "RStudio",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Other_chat_system_IRC_proprietary_software_etc",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "QA_or_test_developer",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Jira",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Trello",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "iOS",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Trello",
                            "weight": "41"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Trello",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Marketing_or_sales_professional",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "Python",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Erlang",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Go",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Haskell",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Python",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "PostgreSQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Haskell",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "PostgreSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "HTML",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Java",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Perl",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "R",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "SQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "R",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Ruby",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "PostgreSQL",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "BSDUnix",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "SAP",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "iOS",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "C",
                            "weight": "22"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Python",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "MySQL",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Windows",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Visual_Basic",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "Marketing_or_sales_professional",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "557"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Java",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Matlab",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Git",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Git",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Swift",
                            "weight": "124"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Swift",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Xcode",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Xcode",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Xcode",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Xcode",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "SQLite",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "QA_or_test_developer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Windows",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "SQLite",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "Android",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "CSS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "HTML",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "TypeScript",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "TypeScript",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "TypeScript",
                            "weight": "30"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "NodeJS",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "NodeJS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Spring",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Designer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Haskell",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "HTML",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "R",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Marketing_or_sales_professional",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "1C",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Android",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "iOS",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "R",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "1C",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "SAP",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Visual_Studio",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "IBM_DB2",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "IBM_DB2",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "Designer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IBM_DB2",
                            "target": "1C",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "BSDUnix",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Educator_or_academic_researcher",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "SAP",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Educator_or_academic_researcher",
                            "target": "SAP",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Linuxbased",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Windows",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Designer",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio_Code",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "VBNET",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Subversion",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Subversion",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Redis",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Android",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Kotlin",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Android_Studio",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Google_BigQuery",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Android_Studio",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Google_BigQuery",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Google_BigQuery",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "Windows",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Facebook",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Android",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Android",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "QA_or_test_developer",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fortran",
                            "target": "Perl",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Lua",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Visual_Studio",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Eclipse",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "TensorFlow",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Eclipse",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "BashShell",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Pascal",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "SQL",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Mercurial",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "BSDUnix",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "PHPStorm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "SQLite",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "SQLite",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "SQLite",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "VBNET",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "VBNET",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "VBNET",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "1C",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Windows",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Jira",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Basic",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "31"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Facebook",
                            "weight": "73"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Facebook",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "SAP",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "DelphiObject_Pascal",
                            "target": "Swift",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "Swift",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Windows",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "MariaDB",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQLite",
                            "target": "System_administrator",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PyCharm",
                            "target": "Confluence",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Confluence",
                            "weight": "68"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "58"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Angular",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Spark",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Spark",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Spark",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Hadoop",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Spark",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MariaDB",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "System_administrator",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "SAP",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "SAP",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "MySQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "IPythonJupiter",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "IPythonJupiter",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "IPythonJupiter",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IPythonJupiter",
                            "target": "PyCharm",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "ObjectiveC",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "iOS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "MacOs",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "Confluence",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Educator_or_academic_researcher",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "SAP",
                            "weight": "64"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Swift",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "React",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "IntelliJ",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "NetBeans",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Eclipse",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IntelliJ",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "QA_or_test_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NetBeans",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "React",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "HTML",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Java",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Python",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "SQL",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Visual_Studio",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "1C",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Matlab",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "React",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "iOS",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "System_administrator",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Clojure",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "CoffeeScript",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "CoffeeScript",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "HTML",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "Python",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "R",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "SQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Clojure",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "HTML",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "R",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "Ruby",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "SQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CoffeeScript",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Rust",
                            "target": "SQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Rust",
                            "target": "BSDUnix",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Rust",
                            "target": "Linuxbased",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Notepad",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Notepad",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Notepad",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Notepad",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Atom",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "1C",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Notepad",
                            "target": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Xcode",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Windows",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Xcode",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xcode",
                            "target": "SAP",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SAP",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Jira",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Hadoop",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Spark",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Spark",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Spark",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Spring",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spring",
                            "target": "TorchPyTorch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TorchPyTorch",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "VBNET",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "VBNET",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "VBNET",
                            "target": "Visual_Basic",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Java",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Haskell",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Visual_Studio",
                            "target": "Zend",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Zend",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MSSQL",
                            "target": "Designer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Backend_developer",
                            "weight": "62"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Frontend_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Frontend_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Pascal",
                            "target": "MacOs",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "F",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Python",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "SQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Django",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "F",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Backend_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "QA_or_test_developer",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "React",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Spark",
                            "weight": "52"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Ruby",
                            "weight": "32"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Amazon_RDSAurora",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_RDSAurora",
                            "target": "PostgreSQL",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_RDSAurora",
                            "target": "Redis",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_RDSAurora",
                            "target": "Linuxbased",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Hadoop",
                            "weight": "27"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Linuxbased",
                            "weight": "111"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Trello",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Angular",
                            "weight": "16"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "C",
                            "target": "Mobile_developer",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "1C",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Embedded_application_or_devices_developer",
                            "target": "Mobile_developer",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Django",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "Mobile_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "HTML",
                            "target": "Mobile_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Mobile_developer",
                            "weight": "13"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Ruby",
                            "weight": "81"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ObjectiveC",
                            "target": "Mobile_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Mobile_developer",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Mobile_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "Mobile_developer",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Android",
                            "weight": "19"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "iOS",
                            "weight": "11"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Hadoop",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Hadoop",
                            "weight": "113"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "SAP",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Spark",
                            "weight": "21"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "QA_or_test_developer",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PHPStorm",
                            "target": "PyCharm",
                            "weight": "44"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Django",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Marketing_or_sales_professional",
                            "target": "SAP",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MariaDB",
                            "target": "NodeJS",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "MySQL",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Data_or_business_analyst",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Spark",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Spark",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Ruby",
                            "weight": "12"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Spring",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Spring",
                            "weight": "107"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Stack_Overflow_Enterprise",
                            "target": "Linuxbased",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Stack_Overflow_Enterprise",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "PostgreSQL",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Hadoop",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Hadoop",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Amazon_RDSAurora",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "Amazon_Redshift",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_RDSAurora",
                            "target": "Amazon_Redshift",
                            "weight": "8"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Xamarin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "QA_or_test_developer",
                            "weight": "48"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Stack_Overflow_Enterprise",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Data_or_business_analyst",
                            "weight": "25"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Backend_developer",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Embedded_application_or_devices_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Embedded_application_or_devices_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Embedded_application_or_devices_developer",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Linuxbased",
                            "weight": "10"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Embedded_application_or_devices_developer",
                            "target": "Linuxbased",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "React",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "Data_or_business_analyst",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TensorFlow",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Hadoop",
                            "weight": "28"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "Hadoop",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Fullstack_developer",
                            "target": "SAP",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Spark",
                            "weight": "14"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Spring",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "QA_or_test_developer",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "MySQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "PostgreSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "SAP",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Mobile_developer",
                            "weight": "17"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Hadoop",
                            "weight": "18"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Csuite_executive_CEO_CTO_etc",
                            "target": "MacOs",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Jira",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Frontend_developer",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Spring",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Android_Studio",
                            "target": "MacOs",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Backend_developer",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Microsoft_Azure_Tables_CosmosDB_SQL_etc",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Office__productivity_suite_Microsoft_Office_Google_Suite_etc",
                            "target": "BSDUnix",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Linuxbased",
                            "weight": "15"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Marketing_or_sales_professional",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Hadoop",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "QA_or_test_developer",
                            "weight": "40"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_Cloud_Storage",
                            "target": "Data_scientist_or_machine_learning_specialist",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Confluence",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Desktop_or_enterprise_applications_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Marketing_or_sales_professional",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Facebook",
                            "weight": "23"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Groovy",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cordova",
                            "target": "Xamarin",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "NodeJS",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "QA_or_test_developer",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "NodeJS",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "QA_or_test_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "IPythonJupiter",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "IPythonJupiter",
                            "target": "Git",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Rust",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Rust",
                            "target": "Data_or_business_analyst",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "Scala",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Angular",
                            "target": "iOS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_DynamoDB",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_RDSAurora",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Amazon_Redshift",
                            "target": "MySQL",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Lua",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Jira",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "PostgreSQL",
                            "target": "DevOps_specialist",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Database_Administrator",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Backend_developer",
                            "target": "Mobile_developer",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hbase",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "Android",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Apache_Hive",
                            "target": "iOS",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Engineering_manager",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_scientist_or_machine_learning_specialist",
                            "target": "Engineering_manager",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Mobile_developer",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "R",
                            "target": "DevOps_specialist",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "NET_Core",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Data_or_business_analyst",
                            "target": "Angular",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Scala",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Erlang",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "CSS",
                            "target": "TypeScript",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "TypeScript",
                            "target": "TensorFlow",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Confluence",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Trello",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Trello",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "QA_or_test_developer",
                            "weight": "39"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Confluence",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "ElasticSearch",
                            "target": "Trello",
                            "weight": "38"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Cassandra",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Google_BigQuery",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "SQL",
                            "target": "Cassandra",
                            "weight": "20"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Google_BigQuery",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Spark",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Groovy",
                            "target": "Git",
                            "weight": "9"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Assembly",
                            "target": "Scala",
                            "weight": "5"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "BSDUnix",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Scala",
                            "weight": "101"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Java",
                            "target": "Neo4j",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Python",
                            "target": "Neo4j",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Neo4j",
                            "target": "Angular",
                            "weight": "6"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "MSSQL",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "Hadoop",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Cassandra",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "ElasticSearch",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Hadoop",
                            "weight": "100"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "QA_or_test_developer",
                            "weight": "7"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Cassandra",
                            "target": "Linuxbased",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "React",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Confluence",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Jira",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Xamarin",
                            "target": "Git",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Confluence",
                            "target": "Other_wiki_tool_Github_Google_Sites_proprietary_software_etc",
                            "weight": "4"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Go",
                            "target": "Kotlin",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Kotlin",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Ruby",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Swift",
                            "target": "Team_Foundation_Version_Control",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Redis",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "NodeJS",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Scala",
                            "target": "Spring",
                            "weight": "102"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Redis",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Spring",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Git",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Google_BigQuery",
                            "target": "Mercurial",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Redis",
                            "target": "Spark",
                            "weight": "1"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Git",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Spark",
                            "target": "Mercurial",
                            "weight": "3"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Hadoop",
                            "target": "Mercurial",
                            "weight": "2"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "BashShell",
                            "target": "ObjectiveC",
                            "weight": "122"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "MySQL",
                            "target": "Facebook",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Django",
                            "target": "Facebook",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Facebook",
                            "target": "Git",
                            "weight": "71"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Desktop_or_enterprise_applications_developer",
                            "target": "QA_or_test_developer",
                            "weight": "43"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Perl",
                            "target": "Csuite_executive_CEO_CTO_etc",
                            "weight": "24"
                        }
                    },
                    {
                        "_attributes": {
                            "source": "Database_Administrator",
                            "target": "Spring",
                            "weight": "19"
                        }
                    }
                ]
            }
        }
    }
}`);

export default data;
