const roleGroups: FilterTaxonomyGroup[] = [
  {
    title: 'Разработчики',
    nodes: [
      { id: 'QA_or_test_developer', title: 'QA or test developer' },
      { id: 'Mobile_developer', title: 'Mobile developer' },
      { id: 'Backend_developer', title: 'Back-end developer' },
      { id: 'Frontend_developer', title: 'Front-end developer' },
      {
        id: 'Desktop_or_enterprise_applications_developer',
        title: 'Desktop or enterprise applications developer',
      },
      {
        id: 'Embedded_application_or_devices_developer',
        title: 'Embedded application or devices developer',
      },
      { id: 'Game_or_graphics_developer', title: 'Game or graphics developer' },
      { id: 'Fullstack_developer', title: 'Full-stack developer' },
    ],
  },
  {
    title: 'Администраторы',
    nodes: [
      { id: 'System_administrator', title: 'System administrator' },
      { id: 'Database_administrator', title: 'Database administrator' },
      { id: 'DevOps_specialist', title: 'DevOps specialist' },
    ],
  },
  {
    title: 'Маркетинг и управление',
    nodes: [
      { id: 'Marketing_or_sales_professional', title: 'Marketing or sales professional' },
      { id: 'Csuite_executive_CEO_CTO_etc', title: 'C-suite executive (CEO, CTO, etc)' },
      { id: 'Engineering_manager', title: 'Engineering manager' },
    ],
  },
  {
    title: 'Исследователи и аналитики',
    nodes: [
      { id: 'Educator_or_academic_researcher', title: 'Educator or academic researcher' },
      {
        id: 'Data_scientist_or_machine_learning_specialist',
        title: 'Data scientist or machine learning specialist',
      },
      { id: 'Data_or_business_analyst', title: 'Data or business analyst' },
    ],
  },
  { title: 'Дизайн', nodes: [{ id: 'Designer', title: 'Designer' }] },
];
export default roleGroups;
